#ifndef SL_EXP_NFS_NFS_OP_HPP
#define SL_EXP_NFS_NFS_OP_HPP

#include <ndn-cxx/util/time.hpp>

namespace ndn {
namespace nfs_trace {

// use system_clock so that logs can be correlated across machines
typedef time::system_clock EmulationClock;
typedef EmulationClock::TimePoint EmulationTime;

enum NfsProc {
  NFS_NONE,
  NFS_GETATTR,
  NFS_LOOKUP,
  NFS_ACCESS,
  NFS_READLINK,
  NFS_READ,
  NFS_WRITE,
  NFS_READDIRP,
  NFS_SETATTR,
  NFS_CREATE,
  NFS_MKDIR,
  NFS_SYMLINK,
  NFS_REMOVE,
  NFS_RMDIR,
  NFS_RENAME
};

static std::vector<std::string> NfsProcStrings = {
  "none",
  "getattr",
  "lookup",
  "access",
  "readlink",
  "read",
  "write",
  "readdirp",
  "setattr",
  "create",
  "mkdir",
  "symlink",
  "remove",
  "rmdir",
  "rename"
};

NfsProc
parseNfsProc(const std::string& s);

typedef uint64_t NfsTimestamp; // microseconds from epoch

NfsTimestamp
parseNfsTimestamp(const std::string& s);

struct NfsOp
{
  NfsTimestamp timestamp;
  NfsProc proc;
  std::string path;
  uint64_t version;
  uint64_t segStart;
  uint64_t nSegments;
};

std::ostream&
operator<<(std::ostream& os, const NfsOp& op);

/** \brief parses .ops trace file
 */
class OpsParser : noncopyable
{
public:
  explicit
  OpsParser(std::istream& is);

  NfsOp
  read();

public:
  std::function<bool(const NfsTimestamp&)> acceptTimestamp;

private:
  std::istream& m_is;
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_NFS_OP_HPP
