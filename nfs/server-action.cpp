#include "server-action.hpp"
#include <sstream>
#include <boost/lexical_cast.hpp>

namespace ndn {
namespace nfs_trace {

ServerActionVerb
parseServerActionVerb(const std::string& s)
{
  auto it = std::find(ServerActionVerbStrings.begin(), ServerActionVerbStrings.end(), s);
  if (it != ServerActionVerbStrings.end()) {
    return static_cast<ServerActionVerb>(it - ServerActionVerbStrings.begin());
  }
  return SA_NONE;
}

ServerAction
ServerAction::fromExclude(const Exclude& exclude)
{
  ServerAction sa = { SA_NONE, 0, 0 };
  if (exclude.size() != 1) {
    return sa;
  }

  const Exclude::Range& excludeRange = *exclude.begin();
  if (!excludeRange.isSingular()) {
    return sa;
  }
  const name::Component& comp = excludeRange.from;
  std::string s(reinterpret_cast<const char*>(comp.value()), comp.value_size());
  size_t pos = s.find(':'), pos2 = std::string::npos;
  std::string verb = (pos == std::string::npos) ? s : s.substr(0, pos);
  sa.verb = parseServerActionVerb(verb);

  switch (sa.verb) {
    case SA_WRITE:
    case SA_FETCH:
    case SA_COMMIT:
    case SA_SIMPLECMD:
      // no arg
      break;
    case SA_ATTR:
    case SA_READLINK:
      // arg1
      sa.arg1 = boost::lexical_cast<uint64_t>(s.substr(pos + 1));
      break;
    case SA_READ:
    case SA_READDIR2:
      // arg2
      sa.arg2 = boost::lexical_cast<size_t>(s.substr(pos + 1));
      break;
    case SA_READDIR1:
      // arg1:arg2
      pos2 = s.rfind(':');
      sa.arg1 = boost::lexical_cast<uint64_t>(s.substr(pos + 1, pos2 - pos - 1));
      sa.arg2 = boost::lexical_cast<size_t>(s.substr(pos2 + 1));
      break;
    default:
      break;
  }
  return sa;
}

Exclude
ServerAction::toExclude() const
{
  std::stringstream ss;
  ss << ServerActionVerbStrings[verb];
  switch (verb) {
    case SA_WRITE:
    case SA_FETCH:
    case SA_COMMIT:
    case SA_SIMPLECMD:
      // no arg
      break;
    case SA_ATTR:
    case SA_READLINK:
      // arg1
      ss << ':' << arg1;
      break;
    case SA_READ:
    case SA_READDIR2:
      // arg2
      ss << ':' << arg2;
      break;
    case SA_READDIR1:
      // arg1:arg2
      ss << ':' << arg1;
      ss << ':' << arg2;
      break;
    default:
      break;
  }
  std::string s = ss.str();
  Exclude exclude;
  exclude.excludeOne(name::Component(reinterpret_cast<const uint8_t*>(s.data()), s.size()));
  return exclude;
}

} // namespace nfs_trace
} // namespace ndn
