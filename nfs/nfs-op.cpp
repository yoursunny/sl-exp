#include "nfs-op.hpp"
#include <boost/lexical_cast.hpp>

namespace ndn {
namespace nfs_trace {

NfsProc
parseNfsProc(const std::string& s)
{
  auto it = std::find(NfsProcStrings.begin(), NfsProcStrings.end(), s);
  if (it != NfsProcStrings.end()) {
    return static_cast<NfsProc>(it - NfsProcStrings.begin());
  }
  return NFS_NONE;
}

NfsTimestamp
parseNfsTimestamp(const std::string& s)
{
  double d = boost::lexical_cast<double>(s);
  return static_cast<NfsTimestamp>(d * 1000000);
}

std::ostream&
operator<<(std::ostream& os, const NfsOp& op)
{
  os << op.timestamp << ','
     << NfsProcStrings[op.proc] << ','
     << op.path << ','
     << op.version << ','
     << op.segStart << ','
     << op.nSegments;
  return os;
}

OpsParser::OpsParser(std::istream& is)
  : acceptTimestamp(bind([] { return true; }))
  , m_is(is)
{
}

NfsOp
OpsParser::read()
{
  while (true) {
    NfsOp op = {NfsTimestamp(), NFS_NONE, "", 0, 0, 0};
    if (m_is.eof()) {
      return op;
    }

    static std::string line;
    std::getline(m_is, line);

    size_t pos1 = line.find(','),
           pos2 = line.find(',', pos1 + 1),
           pos3 = line.find(',', pos2 + 1),
           pos4 = line.find(',', pos3 + 1),
           pos5 = line.find(',', pos4 + 1);
    if (pos1 == std::string::npos ||
        pos2 == std::string::npos ||
        pos3 == std::string::npos ||
        pos4 == std::string::npos ||
        pos5 == std::string::npos) {
      continue; // bad input line
    }

    op.timestamp = parseNfsTimestamp(line.substr(0, pos1));
    if (!acceptTimestamp(op.timestamp)) {
      continue;
    }

    op.proc = parseNfsProc(line.substr(pos1 + 1, pos2 - pos1 - 1));
    if (op.proc == NFS_NONE) {
      continue; // bad input line
    }

    op.path = line.substr(pos2 + 1, pos3 - pos2 - 1);

    if (op.proc == NFS_READ || op.proc == NFS_WRITE || op.proc == NFS_READDIRP) {
      op.version = parseNfsTimestamp(line.substr(pos3 + 1, pos4 - pos3 - 1));
      op.segStart = boost::lexical_cast<uint64_t>(line.substr(pos4 + 1, pos5 - pos4 - 1));
      op.nSegments = boost::lexical_cast<uint64_t>(line.substr(pos5 + 1));
    }

    if (op.version == 0) {
      // TODO reprocess the trace for accurate ctime or mtime
      op.version = op.timestamp;
    }

    return op;
  }
  BOOST_ASSERT(false);
  return {NfsTimestamp(), NFS_NONE, "", 0, 0, 0};
}

} // namespace nfs_trace
} // namespace ndn
