#include "emulation-runner.hpp"

namespace ndn {
namespace nfs_trace {

int
client_main(int argc, char* argv[])
{
  // argv: client-name
  // stdin: ops-trace

  boost::asio::io_service io;
  Face face(io);

  OpsParser trace(std::cin);
  Client client(face, "ndn:/NFS", std::string("ndn:/") + argv[1]);

  EmulationRunner runner(trace, client, io, std::cout);
  runner.onFinish.connect([&] {
    io.stop();
  });
  runner.start();

  io.run();
  return 0;
}

} // namespace nfs_trace
} // namespace ndn

int
main(int argc, char* argv[])
{
  return ndn::nfs_trace::client_main(argc, argv);
}
