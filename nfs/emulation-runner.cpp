#include "emulation-runner.hpp"

namespace ndn {
namespace nfs_trace {

const EmulationClock::Duration EmulationRunner::CLEANUP_INTERVAL = time::seconds(10);
const EmulationClock::Duration EmulationRunner::WAIT_AFTER_LAST_OP = time::seconds(20);

EmulationRunner::EmulationRunner(OpsParser& trace, Client& client,
                                 boost::asio::io_service& io, std::ostream& log)
  : m_io(io)
  , m_trace(trace)
  , m_client(client)
  , m_log(log)
  , m_scheduler(io)
  , m_isStarted(false)
  , m_isInputEnded(false)
  , m_pendings(0)
{
  m_client.opSuccess.connect([this] (const NfsOp& op,
                                     const EmulationTime& start, const EmulationTime& end) {
    BOOST_ASSERT(op.proc != NFS_NONE);
    m_log << op << ','
          << "SUCCESS" << ','
          << time::duration_cast<time::microseconds>(start.time_since_epoch()).count() << ','
          << time::duration_cast<time::microseconds>(end.time_since_epoch()).count() << ','
          << time::duration_cast<time::microseconds>(end - start).count() << std::endl;
    if (--m_pendings == 0 && m_isInputEnded) {
      this->finish();
    }
  });
  m_client.opFailure.connect([this] (const NfsOp& op,
                                     const EmulationTime& start, const EmulationTime& end) {
    BOOST_ASSERT(op.proc != NFS_NONE);
    m_log << op << ','
          << "FAILURE" << ','
          << time::duration_cast<time::microseconds>(start.time_since_epoch()).count() << ','
          << time::duration_cast<time::microseconds>(end.time_since_epoch()).count() << ','
          << time::duration_cast<time::microseconds>(end - start).count() << std::endl;
    if (--m_pendings == 0 && m_isInputEnded) {
      this->finish();
    }
  });
}

void
EmulationRunner::start()
{
  BOOST_ASSERT(!m_isStarted);
  m_isStarted = true;

  m_startEmulationTime = EmulationClock::now();

  this->periodicalCleanupThenReschedule();

  this->run();
}

EmulationTime
EmulationRunner::computeEmulationTime(NfsTimestamp nfsTime) const
{
  return m_startEmulationTime + time::microseconds(nfsTime);
}

void
EmulationRunner::run()
{
  while (true) {
    m_nextOp = m_trace.read();
    if (m_nextOp.proc == NFS_NONE) {
      m_isInputEnded = true;
      if (m_pendings == 0) {
        this->finish();
      }
      return;
    }
    if (m_client.isIgnored(m_nextOp)) {
      continue;
    }

    EmulationClock::Duration slack = this->computeEmulationTime(m_nextOp.timestamp) -
                                     EmulationClock::now();
    ++m_pendings;

    m_log << m_nextOp << ','
          << "SCHED" << ','
          << "slack=" << time::duration_cast<time::microseconds>(slack).count() << ','
          << "pendings=" << m_pendings << ','
          << std::endl;

    if (slack > EmulationClock::Duration::zero()) {
      m_scheduler.scheduleEvent(slack, [this] {
        m_client.startOp(m_nextOp);
        this->run();
      });
      return;
    }
    else {
      m_client.startOp(m_nextOp);
    }
  }
}

void
EmulationRunner::finish()
{
  m_scheduler.cancelEvent(m_periodicalCleanup);
  m_scheduler.scheduleEvent(WAIT_AFTER_LAST_OP, [this] {
    m_client.periodicalCleanup();
    onFinish();
  });
}

void
EmulationRunner::periodicalCleanupThenReschedule()
{
  m_client.periodicalCleanup();
  m_periodicalCleanup = m_scheduler.scheduleEvent(CLEANUP_INTERVAL,
      bind(&EmulationRunner::periodicalCleanupThenReschedule, this));
}

} // namespace nfs_trace
} // namespace ndn
