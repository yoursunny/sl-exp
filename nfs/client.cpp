#include "client.hpp"
#include "fake-signature.hpp"
#include "segment-fetcher.hpp"

namespace ndn {
namespace nfs_trace {

const EmulationClock::Duration Client::FETCH_MAX_GAP = time::seconds(30);

Client::Client(Face& face, const Name& serverPrefix, const Name& clientHost)
  : m_face(face)
  , m_serverPrefix(serverPrefix)
  , m_clientHost(clientHost.toUri())
  , m_clientPrefix(Name(clientHost).append("NFS"))
{
  std::fill_n(m_payloadBuffer, sizeof(m_payloadBuffer), 0xBB);
  m_face.setInterestFilter(m_clientPrefix, bind(&Client::processIncomingInterest, this, _2),
                           bind([] { BOOST_ASSERT_MSG(false, "prefix registration failed"); }));
}

bool
Client::isIgnored(const NfsOp& op) const
{
  switch (op.proc) {
    case NFS_ACCESS:
      return true;
    default:
      return false;
  }
}

void
Client::startOp(const NfsOp& op)
{
  switch (op.proc) {
    case NFS_GETATTR:
    case NFS_LOOKUP:
      this->sendCommand(op, {name::Component("."), name::Component("attr")},
                        ServerAction{SA_ATTR, op.version, 0}, false);
      break;
    case NFS_READLINK:
      this->sendCommand(op, {},
                        ServerAction{SA_READLINK, op.version, 0}, false);
      break;
    case NFS_READ:
      this->startRead(op);
      break;
    case NFS_WRITE:
      this->startWrite(op);
      break;
    case NFS_READDIRP:
      this->startReadDir(op);
      break;
    case NFS_SETATTR:
    case NFS_CREATE:
    case NFS_MKDIR:
    case NFS_SYMLINK:
    case NFS_REMOVE:
    case NFS_RMDIR:
    case NFS_RENAME:
      this->sendCommand(op, {name::Component("."), name::Component(NfsProcStrings[op.proc])},
                        ServerAction{SA_SIMPLECMD, 0, 0}, true);
      break;
    case NFS_ACCESS: // ignored
    default:
      break;
  }
}

void
Client::periodicalCleanup()
{
  this->expireWrites();
}

Interest
Client::makeCommand(const std::string& path, const std::vector<name::Component>& appendToName,
                    const ServerAction& sa, bool needSignature)
{
  Name name(m_serverPrefix);
  name.append(Name(path));
  for (const name::Component& comp : appendToName) {
    name.append(comp);
  }

  if (needSignature) {
    FakeSignature::append(name);
  }

  Interest interest(name);
  interest.setExclude(sa);
  interest.setMustBeFresh(true);
  return interest;
}

void
Client::sendCommand(const NfsOp& op, const std::vector<name::Component>& appendToName,
                    const ServerAction& sa, bool needSignature)
{
  Interest interest = this->makeCommand(op.path, appendToName, sa, needSignature);

  EmulationTime start = EmulationClock::now();
  this->sendInterestAutoRetry(interest,
    bind([=] { this->opSuccess(op, start, EmulationClock::now()); }),
    bind([=] { this->opFailure(op, start, EmulationClock::now()); }));
}

void
Client::sendInterestAutoRetry(Interest interest,
                              const DataCallback& onData, const std::function<void()>& onFail,
                              int nRetries)
{
  interest.setNonce(1);
  interest.refreshNonce();

  std::function<void()> failContinuation = onFail;
  if (nRetries > 0) {
    failContinuation = bind(&Client::sendInterestAutoRetry, this, interest, onData, onFail, nRetries - 1);
  }
  m_face.expressInterest(interest, onData, bind(failContinuation), bind(failContinuation));
}

void
Client::processIncomingInterest(const Interest& interest)
{
  ServerAction sa = ServerAction::fromExclude(interest.getExclude());
  switch (sa.verb) {
    case SA_FETCH:
      this->processFetchInterest(interest, sa);
      break;
    default:
      break;
  }
}

void
Client::startRead(const NfsOp& op)
{
  Interest baseInterest;
  baseInterest.setName(Name(m_serverPrefix).append(PartialName(op.path))
                       .appendVersion(op.version).appendSegment(op.segStart));
  baseInterest.setExclude(ServerAction{SA_READ, 0, SEGMENT_SIZE});
  baseInterest.setTag(make_shared<StopFetchAtSegment>(op.segStart + op.nSegments - 1));

  EmulationTime start = EmulationClock::now();
  SegmentFetcher::fetch(m_face, baseInterest, nullptr,
                        bind([=] { this->opSuccess(op, start, EmulationClock::now()); }),
                        bind([=] { this->opFailure(op, start, EmulationClock::now()); }));
}

void
Client::startReadDir(const NfsOp& op)
{
  Name name(m_serverPrefix);
  name.append(PartialName(op.path));
  name.append(name::Component("."));
  name.append(name::Component("dir"));
  name.appendVersion(op.version);

  Interest firstInterest(name.getPrefix(-1));
  firstInterest.setExclude(ServerAction{SA_READDIR1, op.version, DIR_PER_SEGMENT});
  firstInterest.setMustBeFresh(true);

  Interest baseInterest;
  baseInterest.setName(Name(name).appendSegment(1));
  baseInterest.setExclude(ServerAction{SA_READDIR2, 0, DIR_PER_SEGMENT});
  baseInterest.setTag(make_shared<StopFetchAtSegment>(op.nSegments));

  EmulationTime start = EmulationClock::now();

  this->sendInterestAutoRetry(firstInterest,
    bind([=] {
      if (op.nSegments > 1) {
        SegmentFetcher::fetch(m_face, baseInterest, nullptr,
          bind([=] { this->opSuccess(op, start, EmulationClock::now()); }),
          bind([=] { this->opFailure(op, start, EmulationClock::now()); }));
      }
      else {
        this->opSuccess(op, start, EmulationClock::now());
      }
    }),
    bind([=] { this->opFailure(op, start, EmulationClock::now()); }));
}

void
Client::startWrite(const NfsOp& op)
{
  Name fetchPrefix(m_clientPrefix);
  fetchPrefix.append(Name(op.path));
  fetchPrefix.appendVersion(op.version);
  while (m_writes.count(fetchPrefix) + m_completedWrites.count(fetchPrefix) > 0) {
    // simultaneous WRITEs on same path will affect each other,
    // thus version is incremented
    fetchPrefix = fetchPrefix.getSuccessor();
  }
  uint64_t version = fetchPrefix.at(-1).toVersion();

  WriteProcess& wp = m_writes[fetchPrefix];
  wp.start = EmulationClock::now();
  wp.op = op;
  wp.lastFetch = EmulationTime::max();
  wp.hasWriteReply = false;

  std::stringstream params;
  params << m_clientHost << ':' << version << ':'
         << op.segStart << ':' << (op.segStart + op.nSegments - 1);
  Interest writeCmd = this->makeCommand(op.path,
      {name::Component("."), name::Component("write"), name::Component(params.str())},
      ServerAction{SA_WRITE, version, 0}, true);

  this->sendInterestAutoRetry(writeCmd,
    bind([this, fetchPrefix] {
      WriteProcess& wp = m_writes.at(fetchPrefix);
      wp.lastFetch = EmulationClock::now();
      wp.hasWriteReply = true;
      if (wp.hasWriteReply && wp.fetchedSegments.size() == wp.op.nSegments) {
        this->finishWrite(fetchPrefix);
      }
    }),
    bind([this, fetchPrefix] {
      WriteProcess& wp = m_writes.at(fetchPrefix);
      this->opFailure(wp.op, wp.start, EmulationClock::now());
      m_writes.erase(fetchPrefix);
    }));
}

void
Client::processFetchInterest(const Interest& interest, const ServerAction& sa)
{
  Name fetchPrefix = interest.getName().getPrefix(-1);
  auto it = m_writes.find(fetchPrefix);
  if (it == m_writes.end()) { // not a WRITE in progress

    if (m_completedWrites.count(fetchPrefix) > 0) {
      // It's necessary to keep m_completedWrites in order to answer Interests from NFS server
      // for Data previously lost on client-switch link.
      // XXX m_completedWrites grows indefinitely, but this won't be a problem for short traces.
      this->sendFetchReply(interest);
      return;
    }

    lp::Nack nack(interest);
    nack.setReason(lp::NackReason::NO_ROUTE);
    m_face.put(nack);
    return;
  }

  WriteProcess& wp = it->second;
  wp.lastFetch = EmulationClock::now();
  wp.fetchedSegments.insert(interest.getName().at(-1).toSegment());

  this->sendFetchReply(interest);

  if (wp.hasWriteReply && wp.fetchedSegments.size() == wp.op.nSegments) {
    this->finishWrite(fetchPrefix);
  }
}

void
Client::sendFetchReply(const Interest& interest)
{
  auto data = make_shared<Data>(interest.getName());
  data->setContent(m_payloadBuffer, SEGMENT_SIZE);
  FakeSignature::sign(*data);
  m_face.put(*data);
}

void
Client::finishWrite(const Name& fetchPrefix)
{
  auto it = m_writes.find(fetchPrefix);
  BOOST_ASSERT(it != m_writes.end());
  WriteProcess& wp = it->second;
  BOOST_ASSERT(wp.hasWriteReply && wp.fetchedSegments.size() == wp.op.nSegments);

  NfsOp op = wp.op;
  EmulationTime wpStart = wp.start;
  m_writes.erase(it);
  m_completedWrites.insert(fetchPrefix);
  uint64_t version = fetchPrefix.at(-1).toVersion();

  std::stringstream params;
  params << m_clientHost << ':' << version << ':'
         << op.segStart << ':' << (op.segStart + op.nSegments - 1);
  Interest commitCmd = this->makeCommand(op.path,
      {name::Component("."), name::Component("commit"), name::Component(params.str())},
      ServerAction{SA_COMMIT, version, 0}, true);

  this->sendInterestAutoRetry(commitCmd,
    bind([=] { this->opSuccess(op, wpStart, EmulationClock::now()); }),
    bind([=] { this->opFailure(op, wpStart, EmulationClock::now()); }));
}

void
Client::expireWrites()
{
  EmulationTime minLastFetch = EmulationClock::now() - FETCH_MAX_GAP;
  for (auto it = m_writes.begin(); it != m_writes.end();) {
    if (it->second.lastFetch < minLastFetch) {
      WriteProcess& wp = it->second;
      this->opFailure(wp.op, wp.start, EmulationClock::now());
      it = m_writes.erase(it);
    }
    else {
      ++it;
    }
  }
}

} // namespace nfs_trace
} // namespace ndn
