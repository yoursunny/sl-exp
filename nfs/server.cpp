#include "server.hpp"
#include <boost/lexical_cast.hpp>
#include <ndn-cxx/security/signing-helpers.hpp>
#include <ndn-cxx/util/segment-fetcher.hpp>
#include "fake-signature.hpp"
#include "segment-fetcher.hpp"

#ifndef NFS_NO_SL
#include <ndn-cxx/lp/announcement.hpp>
#include <ndn-cxx/lp/tags.hpp>
#endif // NFS_NO_SL


namespace ndn {
namespace nfs_trace {

#ifndef NFS_NO_SL
typedef SimpleTag<size_t, 9100> MatchedPrefixLengthTag;
#endif // NFS_NO_SL

static std::unordered_set<Name>
computeAncestorPrefixes(const std::unordered_set<Name>& prefixes)
{
  std::unordered_set<Name> sps;
  sps.insert(Name()); // insert ndn:/ first, to simplify the loop below

  for (const Name& prefix : prefixes) {
    for (Name sp = prefix; !sp.empty(); sp = sp.getPrefix(-1)) {
      bool isInserted = sps.insert(sp).second;
      if (!isInserted) { // already inserted from another prefix
        break;
      }
    }
  }
  return sps;
}

Server::Server(Face& face, const Name& registeredPrefix, const std::vector<Name>& servedPrefixes)
  : m_face(face)
  , m_registeredPrefix(registeredPrefix)
  , m_servedPrefixes(servedPrefixes.begin(), servedPrefixes.end())
  , m_ancestorPrefixes(computeAncestorPrefixes(m_servedPrefixes))
{
  std::fill_n(m_payloadBuffer, sizeof(m_payloadBuffer), 0xBB);
  m_face.setInterestFilter(m_registeredPrefix, bind(&Server::processInterest, this, _2),
                           bind([] { BOOST_ASSERT_MSG(false, "prefix registration failed"); }));
}

std::pair<bool, Name>
Server::isServed(const Name& name) const
{
  for (size_t i = 0; i < name.size(); ++i) {
    Name prefix = name.getPrefix(i);
    if (m_servedPrefixes.count(prefix) > 0) {
      return std::make_pair(true, prefix);
    }
  }

  // Interest is not served, find the first NameComponent that doesn't share a prefix of a served prefix.
  for (size_t i = 0; i < name.size(); ++i) {
    Name prefix = name.getPrefix(i);
    if (m_ancestorPrefixes.count(prefix) == 0) {
      return std::make_pair(false, prefix);
    }
  }

  // Interest Name is a shorter prefix of some served prefix.
  return std::make_pair(false, Name());
}

void
Server::processInterest(const Interest& interest)
{
  bool isServed = false;
  Name prefix;
  std::tie(isServed, prefix) = this->isServed(interest.getName());
  if (!isServed) {
    // not served by this server
    lp::Nack nack(interest);
    nack.setReason(lp::NackReason::NO_ROUTE);
#ifndef NFS_NO_SL
    if (!prefix.empty()) {
      nack.getHeader().setPrefixLength(prefix.size());
    }
#endif // NFS_NO_SL
    m_face.put(nack);
    return;
  }

  ServerAction sa = ServerAction::fromExclude(interest.getExclude());
  if (sa.verb != SA_NONE) {
#ifndef NFS_NO_SL
    interest.setTag(make_shared<MatchedPrefixLengthTag>(prefix.size()));
#endif // NFS_NO_SL
    this->processRequest(interest, sa);
  }
}

void
Server::processRequest(const Interest& interest, const ServerAction& sa)
{
  switch (sa.verb) {
  case SA_ATTR:
    this->answerSimple(interest, {name::Component::fromVersion(sa.arg1)}, 84);
    break;
  case SA_READLINK:
    this->answerSimple(interest, {name::Component::fromVersion(sa.arg1)}, 160);
    break;
  case SA_READ:
    this->answerSimple(interest, {}, sa.arg2);
    break;
  case SA_WRITE:
    this->answerSimple(interest, {}, 108);
    this->writeFetch(interest);
    break;
  case SA_FETCH: // fetch is from server to client
    break;
  case SA_COMMIT:
    this->answerSimple(interest, {}, 100);
    break;
  case SA_READDIR1:
    this->answerSimple(interest,
        {name::Component::fromVersion(sa.arg1), name::Component::fromSegment(0)},
        174 * sa.arg2);
    break;
  case SA_READDIR2:
    this->answerSimple(interest, {}, 174 * sa.arg2);
    break;
  case SA_SIMPLECMD:
    this->answerSimple(interest, {}, 248);
    break;
  default:
    break;
  }
}

void
Server::answerSimple(const Interest& interest, const std::vector<name::Component>& appendToName,
                     size_t payloadSize)
{
  Name dataName = interest.getName();
  for (const name::Component& comp : appendToName) {
    dataName.append(comp);
  }

  auto data = make_shared<Data>(dataName);
  data->setContent(m_payloadBuffer, std::min(payloadSize, sizeof(m_payloadBuffer)));
  FakeSignature::sign(*data);

#ifndef NFS_NO_SL
  shared_ptr<MatchedPrefixLengthTag> matchedPrefixLengthTag = interest.getTag<MatchedPrefixLengthTag>();
  BOOST_ASSERT(matchedPrefixLengthTag != nullptr);
  lp::Announcement ann;
  ann.setName(interest.getName().getPrefix(*matchedPrefixLengthTag));
  data->setTag(make_shared<lp::AnnouncementTag>(ann));
#endif // NFS_NO_SL

  m_face.put(*data);
}

void
Server::writeFetch(const Interest& interest)
{
  const size_t N_COMMAND_COMPONENTS = 3;
  // "./write/{client-host}:{%FD mtime}:{%00 first-seg}:{%00 last-seg}"

  if (interest.getName().size() <
      m_registeredPrefix.size() + N_COMMAND_COMPONENTS + ndn::command_interest::MIN_SIZE) {
    return;
  }

  Name command = FakeSignature::strip(interest.getName());
  const name::Component& params = command.at(-1);
  std::string s(reinterpret_cast<const char*>(params.value()), params.value_size());
  size_t pos1 = s.find(':'), pos2 = s.find(':', pos1 + 1), pos3 = s.find(':', pos2 + 1);
  if (pos3 == std::string::npos || pos3 != s.rfind(':')) {
    return;
  }

  Name client(s.substr(0, pos1));
  uint64_t mtime = boost::lexical_cast<uint64_t>(s.substr(pos1 + 1, pos2 - pos1 - 1));
  uint64_t first = boost::lexical_cast<uint64_t>(s.substr(pos2 + 1, pos3 - pos2 - 1));
  uint64_t last  = boost::lexical_cast<uint64_t>(s.substr(pos3 + 1));

  PartialName path = command.getSubName(m_registeredPrefix.size(),
                                        command.size() - N_COMMAND_COMPONENTS - m_registeredPrefix.size());

  Interest baseInterest;
  baseInterest.setName(Name(client).append("NFS").append(path).appendVersion(mtime).appendSegment(first));
  baseInterest.setExclude(ServerAction{SA_FETCH, 0, 0});
  baseInterest.setTag(make_shared<StopFetchAtSegment>(last));
  SegmentFetcher::fetch(m_face, baseInterest, nullptr, bind([]{}), bind([]{}));
}

} // namespace nfs_trace
} // namespace ndn
