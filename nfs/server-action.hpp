#ifndef SL_EXP_NFS_SERVER_ACTION_HPP
#define SL_EXP_NFS_SERVER_ACTION_HPP

#include <ndn-cxx/exclude.hpp>

namespace ndn {
namespace nfs_trace {

enum ServerActionVerb {
  SA_NONE      = 0,
  SA_ATTR      = 1,
  SA_READLINK  = 2,
  SA_READ      = 3,
  SA_WRITE     = 4,
  SA_FETCH     = 5,
  SA_COMMIT    = 6,
  SA_READDIR1  = 7,
  SA_READDIR2  = 8,
  SA_SIMPLECMD = 9
};

static std::vector<std::string> ServerActionVerbStrings = {
  "NONE",
  "ATTR",
  "READLINK",
  "READ",
  "WRITE",
  "FETCH",
  "COMMIT",
  "READDIR1",
  "READDIR2",
  "SIMPLECMD"
};

inline ServerActionVerb
parseServerActionVerb(const std::string& s);

class ServerAction
{
public:
  static ServerAction
  fromExclude(const Exclude& exclude);

  Exclude
  toExclude() const;

  /** \brief implicitly convertible to Exclude,
   *         so that one can write interest.setExclude(ServerAction{...})
   */
  operator Exclude() const
  {
    return this->toExclude();
  }

public:
  ServerActionVerb verb;
  uint64_t arg1;
  size_t arg2;
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_SERVER_ACTION_HPP
