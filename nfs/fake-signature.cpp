#include "fake-signature.hpp"
#include <ndn-cxx/security/signature-sha256-with-rsa.hpp>

namespace ndn {
namespace nfs_trace {

Name&
FakeSignature::append(Name& interestName)
{
  static const name::Component SIG1("TIME");
  static const name::Component SIG2("NONC");
  static const name::Component SIG3("SIGNATURE-INFO-SIGNATURE-INFO-SIGNATURE-INFO");
  static const name::Component SIG4("SIGNATURE-VALUE-SIGNATURE-VALUE-"
                                    "SIGNATURE-VALUE-SIGNATURE-VALUE");
  return interestName.append(SIG1).append(SIG2).append(SIG3).append(SIG4);
}

Name
FakeSignature::strip(const Name& interestName)
{
  return interestName.getPrefix(-4);
}

Data&
FakeSignature::sign(Data& data)
{
  SignatureSha256WithRsa fakeSignature;
  fakeSignature.setValue(encoding::makeEmptyBlock(tlv::SignatureValue));
  data.setSignature(fakeSignature);
  data.wireEncode();
  return data;
}

} // namespace nfs_trace
} // namespace ndn
