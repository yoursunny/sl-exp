#ifndef SL_EXP_NFS_FAKE_SIGNATURE_HPP
#define SL_EXP_NFS_FAKE_SIGNATURE_HPP

#include <ndn-cxx/data.hpp>

namespace ndn {
namespace nfs_trace {

/** \brief add or remove fake signature
 */
class FakeSignature
{
public:
  static Name&
  append(Name& interestName);

  static Name
  strip(const Name& interestName);

  static Data&
  sign(Data& data);
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_FAKE_SIGNATURE_HPP
