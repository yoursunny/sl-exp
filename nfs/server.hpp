#ifndef SL_EXP_NFS_SERVER_HPP
#define SL_EXP_NFS_SERVER_HPP

#include <unordered_set>
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include "server-action.hpp"

namespace ndn {
namespace nfs_trace {

class Server : noncopyable
{
public:
  /** \param prefix prefix to register with forwarder
   *  \param prefixes prefix to serve
   */
  Server(Face& face, const Name& registeredPrefix, const std::vector<Name>& servedPrefixes);

private:
  /** \brief determines whether this server should serve an Interest
   *  \return (true,matchedPrefix) if Interest should be served, or
   *          (false,Name()) if Interest should not be served but cannot be Nacked with a prefix, or
   *          (false,nackedPrefix) if Interest should be Nacked with specified prefix
   */
  std::pair<bool, Name>
  isServed(const Name& name) const;

  void
  processInterest(const Interest& interest);

  /** \brief answers a request from trace replay
   */
  void
  processRequest(const Interest& interest, const ServerAction& sa);

  void
  answerSimple(const Interest& interest, const std::vector<name::Component>& appendToName,
               size_t payloadSize);

  /** \brief fetches from client after a WRITE request
   */
  void
  writeFetch(const Interest& interest);

private:
  Face& m_face;
  const Name m_registeredPrefix;
  const std::unordered_set<Name> m_servedPrefixes;
  const std::unordered_set<Name> m_ancestorPrefixes;
  uint8_t m_payloadBuffer[ndn::MAX_NDN_PACKET_SIZE];
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_SERVER_HPP
