#ifndef SL_EXP_NFS_CLIENT_HPP
#define SL_EXP_NFS_CLIENT_HPP

#include <unordered_set>
#include <unordered_map>
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/util/signal.hpp>
#include "nfs-op.hpp"
#include "server-action.hpp"

namespace ndn {
namespace nfs_trace {

using ndn::util::signal::Signal;

class Client : noncopyable
{
public:
  /** \param serverPrefix Name prefix of NFS servers
   *  \param clientHost prefix of this client host
   */
  Client(Face& face, const Name& serverPrefix, const Name& clientHost);

  bool
  isIgnored(const NfsOp& op) const;

  void
  startOp(const NfsOp& op);

  /** \brief call this periodically, and sometime after last operation,
   *         to clean up internal states
   */
  void
  periodicalCleanup();

private:
  Interest
  makeCommand(const std::string& path, const std::vector<name::Component>& appendToName,
              const ServerAction& sa, bool needSignature);

  /** \brief send a single-Interest command
   */
  void
  sendCommand(const NfsOp& op, const std::vector<name::Component>& appendToName,
              const ServerAction& sa, bool needSignature);

  void
  sendInterestAutoRetry(Interest interest,
                        const DataCallback& onData, const std::function<void()>& onFail,
                        int nRetries = 3);

  void
  processIncomingInterest(const Interest& interest);

  void
  startRead(const NfsOp& op);

  void
  startReadDir(const NfsOp& op);

private: // WRITE
  struct WriteProcess
  {
    EmulationTime start;
    NfsOp op;
    EmulationTime lastFetch;
    bool hasWriteReply;
    std::set<uint64_t> fetchedSegments;
  };

  void
  startWrite(const NfsOp& op);

  void
  processFetchInterest(const Interest& interest, const ServerAction& sa);

  void
  sendFetchReply(const Interest& interest);

  void
  finishWrite(const Name& fetchPrefix);

  /** \brief fail WRITEs where no fetch Interest has been received within FETCH_MAX_GAP
   */
  void
  expireWrites();

public:
  Signal<Client, NfsOp, EmulationTime, EmulationTime> opSuccess;
  Signal<Client, NfsOp, EmulationTime, EmulationTime> opFailure;

private:
  Face& m_face;
  Name m_serverPrefix;
  std::string m_clientHost;
  Name m_clientPrefix;
  uint8_t m_payloadBuffer[ndn::MAX_NDN_PACKET_SIZE];
  std::unordered_map<Name, WriteProcess> m_writes;
  std::unordered_set<Name> m_completedWrites;
  static const int SEGMENT_SIZE = 4096;
  static const int DIR_PER_SEGMENT = 32;
  static const EmulationClock::Duration FETCH_MAX_GAP;
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_CLIENT_HPP
