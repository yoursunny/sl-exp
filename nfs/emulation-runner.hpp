#ifndef SL_EXP_NFS_EMULATION_RUNNER_HPP
#define SL_EXP_NFS_EMULATION_RUNNER_HPP

#include "client.hpp"
#include <ndn-cxx/util/scheduler.hpp>

namespace ndn {
namespace nfs_trace {

/** \brief drives an emulation session
 */
class EmulationRunner : noncopyable
{
public:
  EmulationRunner(OpsParser& trace, Client& client,
                  boost::asio::io_service& io, std::ostream& log);

  /** \brief start replaying the trace
   */
  void
  start();

  Signal<EmulationRunner> onFinish;

private:
  EmulationTime
  computeEmulationTime(NfsTimestamp nfsTime) const;

  void
  run();

  /** \brief no more operations
   */
  void
  finish();

  void
  periodicalCleanupThenReschedule();

private:
  boost::asio::io_service& m_io;
  OpsParser& m_trace;
  Client& m_client;
  std::ostream& m_log;

  Scheduler m_scheduler;
  EventId m_periodicalCleanup;
  static const EmulationClock::Duration CLEANUP_INTERVAL;
  static const EmulationClock::Duration WAIT_AFTER_LAST_OP;

  bool m_isStarted;
  EmulationTime m_startEmulationTime;

  NfsOp m_nextOp;
  bool m_isInputEnded;
  int m_pendings;
};

} // namespace nfs_trace
} // namespace ndn

#endif // SL_EXP_NFS_EMULATION_RUNNER_HPP
