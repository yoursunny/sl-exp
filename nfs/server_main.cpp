#include "server.hpp"
#include <fstream>

namespace ndn {
namespace nfs_trace {

int
server_main(int argc, char* argv[])
{
  // argv: paths-file

  std::vector<Name> prefixes;
  std::ifstream pathsFile(argv[1]);
  std::string path;
  while (pathsFile >> path) {
    prefixes.push_back("ndn:/NFS" + path);
  }

  Face face;
  Server server(face, "ndn:/NFS", prefixes);

  face.processEvents();
  return 0;
}

} // namespace nfs_trace
} // namespace ndn

int
main(int argc, char* argv[])
{
  return ndn::nfs_trace::server_main(argc, argv);
}
