#!/bin/bash
R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

sudo apt-get update
sudo apt-get dist-upgrade -y -qq
sudo apt-get install -y -qq git build-essential gdb valgrind libcrypto++-dev libssl-dev libsqlite3-dev libboost-all-dev pkg-config libpcap-dev doxygen graphviz python-sphinx python-pip
sudo pip install sphinxcontrib-doxylink sphinxcontrib-googleanalytics

cd
git clone https://github.com/mininet/mininet.git
git clone https://github.com/yoursunny/mnndn.git
git clone https://github.com/named-data/ndn-cxx.git
git clone https://github.com/named-data/NFD.git
git clone https://github.com/named-data/ndn-tools.git

pushd mininet
git checkout $(cat $R/install/mininet.commit)
util/install.sh -fnv
popd

pushd mnndn
git checkout $(cat $R/install/mnndn.commit)
popd

pushd ndn-cxx
git checkout $(cat $R/install/ndn-cxx.commit)
patch -p1 < $R/install/ndn-cxx.patch
./waf configure
./waf
sudo ./waf install
sudo ldconfig
popd

pushd NFD
git checkout $(cat $R/install/NFD.commit)
patch -p1 < $R/install/NFD.patch
git submodule update --init
./waf configure
./waf
sudo ./waf install
popd

pushd ndn-tools
git checkout $(cat $R/install/ndn-tools.commit)
patch -p1 < $R/install/ndn-tools.patch
./waf configure
./waf
sudo ./waf install
popd

pushd $R/nfs
make
sudo make install
popd
