#!/usr/bin/python2
"""List hosts in a topology."""

from exp_topo import makeTopo

def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description='List hosts in a topology.')
    parser.add_argument('--topo', required=True,
                        help='topology configuration string')
    parser.add_argument('--sw', action='store_true',
                        help='switches only')
    parser.add_argument('--endhost', action='store_true',
                        help='end hosts only')
    args = parser.parse_args()

    if not args.sw and not args.endhost:
        args.sw = args.endhost = True

    return args

if __name__ == '__main__':
    args = parseCommandLine()
    expTopo = makeTopo(*args.topo.split(','))
    hosts = []
    if args.sw:
        hosts += set(expTopo['topo'].nodes(False)) - set(expTopo['endhosts'])
    if args.endhost:
        hosts += expTopo['endhosts']
    for h in sorted(hosts):
        print h
