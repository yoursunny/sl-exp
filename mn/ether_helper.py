class EtherHelper:
    """Helper for Ethernet-style self-learning emulation."""

    def __init__(self, net, sched, lf):
        """net: a Mininet instance
           sched: a sched.scheduler instance
           lf: a LinkFailure instance"""
        self.net = net
        self.sched = sched
        self.lf = lf

    def parseArg(self, arg):
        """Parse command line arguments.
           arg: t,h1-h2:h3-h4,h5-h6:h7-h8,h9:h10
                At timestamp t, h1-h2 and h3-h4 links are removed from the spanning tree,
                h5-h6 and h7-h8 links are added to the spanning tree,
                and Topology Change Notifications are flooded from h9 and h10."""
        t, offStLinks, onStLinks, tcnNodes = arg.split(',')
        self.changeSt(float(t),
                      [] if offStLinks == '' else [ link.split('-') for link in offStLinks.split(':') ],
                      [] if onStLinks == '' else [ link.split('-') for link in onStLinks.split(':') ],
                      [] if tcnNodes == '' else [ node for node in tcnNodes.split(':') ])

    def changeSt(self, t, offStLinks, onStLinks, tcnNodes):
        """Change spanning tree.
           t: timestamp
           offStLinks: links removed from the spanning tree
           onStLinks: links added to the spanning tree
           tcnLinks: nodes flooding TCN"""
        for (h1, h2) in offStLinks:
            self.lf.fail(h1, h2, t)
        for (h1, h2) in onStLinks:
            self.lf.recover(h1, h2, t)
        for h1 in tcnNodes:
            self.sched.enter(t + 0.001, 0, self._sendTcn, (t, h1))
        self.sched.enter(t, 0, self._log, (t, offStLinks, onStLinks, tcnNodes))

    def _log(self, t, offStLinks, onStLinks, tcnNodes):
        print '%0.6f: Change Ethernet spanning tree: remove [%s], add [%s], send TCN from [%s].' % (
                t,
                ','.join([ '%s-%s' % (h1, h2) for (h1, h2) in offStLinks ]),
                ','.join([ '%s-%s' % (h1, h2) for (h1, h2) in onStLinks ]),
                ','.join(tcnNodes)
              )

    def _sendTcn(self, t, h1):
        import random
        self.net[h1].pexec('ndnpeek', '-w', '1', 'ndn:/%%C1.EtherStrategy~TCN/%d' % random.randint(0, 99999999))
        print '%0.6f: Initiate EtherStrategy topology change notification at %s.' % (t, h1)
