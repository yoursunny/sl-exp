#!/usr/bin/python2
"""Export topology to VDE-FSTP input file."""

import sys
from mnndn.topo import toTopoFile
from exp_topo import makeTopo

def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description='List hosts in a topology.')
    parser.add_argument('--topo', required=True,
                        help='topology configuration string')
    parser.add_argument('--root',
                        help='root bridge')
    args = parser.parse_args()

    return args

def toVde(expTopo, rootBridge, out):
    topo = expTopo['topo']
    if rootBridge is not None:
        topo.nodeInfo(rootBridge)['mnndn'] = dict(root='1')
    toTopoFile(topo, out, excludeHosts=expTopo['endhosts'])

if __name__ == '__main__':
    args = parseCommandLine()
    expTopo = makeTopo(*args.topo.split(','))
    toVde(expTopo, args.root, sys.stdout)
