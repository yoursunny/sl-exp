#!/bin/bash
# clean up /tmp/mnndn and move log files up for easier access

sudo rm -rf /tmp/mnndn/*/root/.ndn /tmp/mnndn/*/var/run
sudo chown -R $(id -nu):$(id -ng) /tmp/mnndn

for H in $(ls -d /tmp/mnndn/*/); do
  pushd $H >/dev/null
  mv var/log/ndn/* ./
  mv etc/ndn/* ./
  rm -rf etc var root
  popd >/dev/null
done
