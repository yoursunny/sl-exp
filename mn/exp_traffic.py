#!/usr/bin/python2
"""Traffic patterns for experiment."""

import time

from mnndn.app import NdnPing,NdnPingServer

class Traffic(object):
    ARGUMENTS_DESCRIPTION = ''

    def start(self, net, sched):
        """Start traffic."""
        sched.enter(self.delay, 0, self.start0, (net, sched))

    def start0(self, net, sched):
        """Start traffic immediately."""
        raise NotImplementedError

    def stop(self, net, sched):
        """Stop traffic immediately."""
        raise NotImplementedError

class CliTraffic(Traffic):
    """This special traffic type can be invoked as `--traffic=cli --duration=0`.
       It opens CLI after topology is constructed, for manual testing."""

    def __init__(self, *opts):
        pass

    def start0(self, net, sched):
        from mininet.cli import CLI
        CLI(net)

    def stop(self, net, sched):
        pass

class PingClientTraffic(Traffic):
    ARGUMENTS_DESCRIPTION = '[interval,count,retx,logname,seqgap,]/prefix,clientHost,clientHost,..'

    def __init__(self, *opts):
        if len(opts) < 2:
            raise IndexError('PingClientTraffic needs at least /prefix,clientHost')

        self.interval = NdnPing.DEFAULT_INTERVAL
        """Interval (in milliseconds) between probes."""
        if opts[0][0] != '/':
            self.interval = int(opts[0])
            opts = opts[1:]

        self.count = None
        """Count of probes sent from each client."""
        if opts[0][0] != '/':
            self.count = int(opts[0])
            opts = opts[1:]

        self.retx = 0
        """Maximum count of retransmissions of a probe upon Nack."""
        if opts[0][0] != '/':
            self.retx = int(opts[0])
            opts = opts[1:]

        self.logname = 'ndnping.log'
        """Log file name."""
        if opts[0][0] != '/':
            self.logname = opts[0]
            opts = opts[1:]

        self.seqgap = 0
        """Gap between starting sequence numbers. If specified, client identifier is disabled."""
        if opts[0][0] != '/':
            self.seqgap = int(opts[0])
            opts = opts[1:]
        self.enableClientId = (self.seqgap == 0)

        if len(opts) < 2:
            raise IndexError('PingClientTraffic needs at least /prefix,clientHost')
        self.prefix = opts[0]
        self.clientHosts = opts[1:]

    def start0(self, net, sched):
        print 'Start ndnping clients for %s.' % self.prefix
        self.clients = {}
        for clientHost in self.clientHosts:
            client = NdnPing(net[clientHost], self.prefix,
                             interval=self.interval, count=self.count, clientId=self.enableClientId,
                             cmdArgs=' '.join(self.makeCmdArgs()))
            self.clients[clientHost] = client
            client.start('/var/log/ndn/%s' % self.logname)

    def makeCmdArgs(self):
        cmdArgs = []
        if self.retx > 0:
            cmdArgs.append('--retx %d' % self.retx)
        if self.seqgap > 0:
            cmdArgs.append('-n %d' % (self.seqgap * (len(self.clients) + 1)))
        return cmdArgs

    def stop(self, net, sched):
        print 'Stop ndnping clients for %s.' % self.prefix
        for client in self.clients.itervalues():
            client.stop()

class ZipfClientTraffic(PingClientTraffic):
    ARGUMENTS_DESCRIPTION = 'zipfN,zipfS,' + PingClientTraffic.ARGUMENTS_DESCRIPTION

    def __init__(self, zipfN, zipfS, *opts):
        self.zipfN = int(zipfN)
        self.zipfS = zipfS # keep as str
        float(zipfS) # but check zipfS is valid float
        super(ZipfClientTraffic, self).__init__(*opts)
        self.enableClientId = False

    def makeCmdArgs(self):
        cmdArgs = super(ZipfClientTraffic, self).makeCmdArgs()
        cmdArgs += ['--zipf-n', str(self.zipfN)]
        cmdArgs += ['--zipf-s', self.zipfS]
        return cmdArgs

class PingServerTraffic(Traffic):
    ARGUMENTS_DESCRIPTION = '/prefix,serverHost[,payloadSize]'

    def __init__(self, prefix, serverHost, payloadSize=1024):
        self.prefix = prefix
        self.serverHost = serverHost
        self.payloadSize = payloadSize

    def start0(self, net, sched):
        print 'Start ndnping server for %s.' % self.prefix
        self.server = NdnPingServer(net[self.serverHost], self.prefix,
                                    payloadSize=self.payloadSize,
                                    freshnessPeriod=86400000)
        self.server.start()

    def stop(self, net, sched):
        print 'Stop ndnping server for %s.' % self.prefix
        self.server.stop()

class NfsTraffic(Traffic):
    ARGUMENTS_DESCRIPTION = 's:serverHost:paths-file,..,c:clientHost:client-name:ops-trace,..,rewrite-timestamp:t*2'

    def __init__(self, *opts):
        self.servers = {}
        self.clients = {}
        self.rewriteTimestamp = None

        addFuncs = {
          's': self.__addServer,
          'c': self.__addClient,
          'rewrite-timestamp': self.__setRewriteTimestamp
        }
        for opt in opts:
            opta = opt.split(':')
            addFunc = addFuncs.get(opta[0], None)
            if addFunc is None:
                raise TypeError
            addFunc(*opta[1:])

    def __addServer(self, host, pathsFile):
        self.servers[host] = dict(pathsFile=pathsFile)

    def __addClient(self, host, clientName, opsTrace):
        clientName = clientName.lstrip('/')
        self.clients[host] = dict(clientName=clientName, opsTrace=opsTrace)

    def __setRewriteTimestamp(self, expr):
        """Set an expression of 't' to rewrite the timestamp.
           This is passed to awk(1)."""
        self.rewriteTimestamp = expr

    def start0(self, net, sched):
        self.startServer(net)
        sched.enter(3, 0, self.startClients, (net,))

    def startServer(self, net):
        print 'Start NFS servers.'
        for hostName, d in self.servers.iteritems():
            host = net[hostName]
            d['proc'] = host.popen('nfs-trace-server', d['pathsFile'])

    def startClients(self, net):
        print 'Start NFS clients.'
        for hostName, d in self.clients.iteritems():
            host = net[hostName]
            if self.rewriteTimestamp is None:
                cmd = 'nfs-trace-client %s < %s > %s' % (d['clientName'], d['opsTrace'], '/var/log/ndn/nfs-trace-client.log')
            else:
                cmd = 'awk \'BEGIN{FS=OFS=","}{t=$1;$1=%s;print}\' %s | nfs-trace-client %s > %s' % (
                      self.rewriteTimestamp, d['opsTrace'], d['clientName'], '/var/log/ndn/nfs-trace-client.log')
            d['proc'] = host.popen('bash', '-c', cmd)

    def stop(self, net, sched):
        self.stopClients()
        sched.enter(0.5, 0, self.stopServer, ())

    def stopServer(self):
        print 'Stop NFS servers.'
        for d in self.servers.itervalues():
            if 'proc' not in d:
                continue
            d['proc'].kill()
            d.pop('proc').wait()

    def stopClients(self):
        print 'Stop NFS clients.'
        for d in self.clients.itervalues():
            if 'proc' not in d:
                continue
            d['proc'].kill()
            d.pop('proc').wait()


TRAFFIC_CTORS = {
  'cli': CliTraffic,
  'pingc': PingClientTraffic,
  'zipf': ZipfClientTraffic,
  'pings': PingServerTraffic,
  'nfs': NfsTraffic,
}

def makeTraffic(*opts):
    """Create traffic pattern from arguments."""
    key = opts[0]
    opts = opts[1:]
    try:
        delay = float(key)
        key = opts[0]
        opts = opts[1:]
    except ValueError:
        delay = 0

    cls = TRAFFIC_CTORS.get(key, None)
    if cls is None:
        raise KeyError('unknown traffic key ' + key)

    try:
        traffic = cls(*opts)
    except TypeError:
        raise IndexError('traffic ' + key + ' expects arguments: ' + cls.ARGUMENTS_DESCRIPTION)

    traffic.delay = delay
    return traffic
