#!/usr/bin/python2
"""Topologies for experiment."""

def makeHostLinearTopo(k):
    k = int(k)
    from mnndn.topo import HostLinearTopo
    topo = HostLinearTopo(k)
    return {
      'topo': topo,
      'endhosts': ['h0', 'h%d'%(k-1)]
    }

def makeGridSpikeTopo(nRows, nCols, nSpikes):
    nRows, nCols, nSpikes = int(nRows), int(nCols), int(nSpikes)
    from mnndn.topo import GridSpikeTopo
    topo = GridSpikeTopo(nRows, nCols, nSpikes)
    return {
      'topo': topo,
      'endhosts': list(topo.spikeNodes())
    }

def makeTreeTopo(height, degree):
    height, degree = int(height), int(degree)
    from mnndn.topo import TreeTopo
    topo = TreeTopo(height, degree)
    return {
      'topo': topo,
      'endhosts': [n for n in topo.hosts() if topo.nodeInfo(n)['mnndn_tree_depth'] == height]
    }

def makeArizonaCsTopo(hosts):
    hosts = hosts.split(':')
    hosts = hosts if len(hosts) > 0 else None

    from arizona_cs_topo import ArizonaCsTopo
    topo = ArizonaCsTopo(hosts=hosts)
    return {
      'topo': topo,
      'endhosts': topo.listEndhosts()
    }

TOPOLOGY_MAKERS = {
  'linear': makeHostLinearTopo,
  'gridspike': makeGridSpikeTopo,
  'tree': makeTreeTopo,
  'arizonacs': makeArizonaCsTopo
}

def makeTopo(key, *opts):
    """Create topology from arguments.
       Returns {'topo':the topology, 'endhosts':[ end host name, ]}"""
    f = TOPOLOGY_MAKERS.get(key, None)
    if f is None:
        raise KeyError('unknown topology key' + key)

    if len(opts) != f.func_code.co_argcount:
        import inspect
        raise IndexError('topology ' + opts[0] + ' expects ' + f.func_code.co_argcount + ' arguments: ' +
                         ','.join(inspect.getargspec(f).args))

    return f(*opts)

def addGateway(expTopo, host):
    """Add an external traffic gateway connected to a host."""
    topo = expTopo['topo']
    topo.addHost('gateway')
    topo.addLink(host, 'gateway')
    topo.addHost('external')
    topo.addLink('gateway', 'external')
