#!/usr/bin/python2
"""Test SlStrategy over a linear topology."""

import time
import functools

from mininet.log import setLogLevel
from mininet.net import Mininet

from mnndn.ndn import NdnHost
from mnndn.topo import HostLinearTopo,assignIps
from mnndn.app import NdnPing,NdnPingServer

def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description='Test SlStrategy over a linear topology.')
    parser.add_argument('--k', type=int, default=3,
                        help='number of hosts')
    parser.add_argument('--duration', type=int, default=60,
                        help='duration of emulation')
    parser.add_argument('--prefix', type=str, default='/P',
                        help='NDN name')
    args = parser.parse_args()

    return args

def run(args):
    topo = HostLinearTopo(args.k)
    hostNames = list(topo.hosts())
    net = Mininet(topo, controller=None,
                  host=functools.partial(NdnHost, rout=None))
    net.start()
    assignIps(net)

    print 'Start forwarding.'
    for hostName in hostNames:
        fw = net[hostName].getFw()
        fw.setLog('DEBUG', 'Forwarder', 'SlStrategy', 'SlFib')
        fw.setStrategy('/', '/localhost/nfd/strategy/sl')
        fw.hasUdpMcast = False
        fw.start()
    time.sleep(5)

    print 'Create faces and default route.'
    for i in range(len(hostNames)-1):
        host = net[hostNames[i]]
        connections = host.connectionsTo(net[hostNames[i+1]])
        fw = host.getFw()
        face = fw.addFace(connections[0][0], connections[0][1])
        if i == 0: # default route for client host
            fw.addRoute(face, '/')

    print 'Start ndnping server.'
    pingServer = NdnPingServer(net[hostNames[-1]], args.prefix)
    pingServer.start()
    time.sleep(1)

    print 'Start ndnping client.'
    pingClient = NdnPing(net[hostNames[0]], args.prefix)
    pingClient.start('/var/log/ndn/ndnping.log')

    time.sleep(args.duration)

    print 'Stop.'
    pingClient.stop()
    time.sleep(1)
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    args = parseCommandLine()
    run(args)
