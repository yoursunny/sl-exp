#!/bin/bash
# Prepare NFS experiment on ArizonaCsTopo.
R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

NFS_FIND_ONLY=1 $R/mn/nfs.sh
if [[ $? -ne 0 ]]; then
  echo 'nfs.sh fails.'
  exit 1
fi

# make topology and traffic configuration
echo -n arizonacs,c00c4508:ac1001e7 > /tmp/nfs-topo-arizonacs.txt
awk '{ printf ":%s", $1 }' /tmp/nfs-client-names.txt >> /tmp/nfs-topo-arizonacs.txt

echo -n nfs > /tmp/nfs-traffic-arizonacs.txt
echo -n ,s:c00c4508:${NFS_DATASET}/zuni/all.paths >> /tmp/nfs-traffic-arizonacs.txt
echo -n ,s:ac1001e7:${NFS_DATASET}/titan/all.paths >> /tmp/nfs-traffic-arizonacs.txt
awk '{ printf ",c:%s:%s:%s.%s.ops", $1, $1, "'$NFS_DATASET/replay/$NFS_TIMEPERIOD'", $1 }' /tmp/nfs-client-names.txt >> /tmp/nfs-traffic-arizonacs.txt


echo 'Topology and traffic configuration have been generated.'
echo '--topo $(cat /tmp/nfs-topo-arizonacs.txt) --traffic $(cat /tmp/nfs-traffic-arizonacs.txt)'
