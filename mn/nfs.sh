#!/bin/bash
# Prepare NFS experiment.
R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

if [[ -z $NFS_DATASET ]] || [[ ! -f $NFS_DATASET/traceinfo.csv ]]; then
  echo '$NFS_DATASET is unspecified or traceinfo.csv does not exist.'
  exit 2
fi

if [[ -z $NFS_TIMEPERIOD ]]; then
  echo '$NFS_TIMEPERIOD is unspecified.'
  exit 2
fi

# find servers
cut -d, -f1 $NFS_DATASET/traceinfo.csv > /tmp/nfs-server-names.txt
NSERVERS=$(cat /tmp/nfs-server-names.txt | wc -l)
if [[ $NSERVERS -eq 0 ]]; then
  echo 'No server is found.'
  exit 2
fi
echo $NSERVERS 'servers found.'

# find clients with significant activity
NFS_MIN=${NFS_MIN:-0}
NFS_MAX=${NFS_MAX:-99999999}
wc -l $NFS_DATASET/replay/$NFS_TIMEPERIOD.*.ops | \
awk '$2!="total" && $1>='$NFS_MIN' && $1<'$NFS_MAX' { print }' | \
sed -e 's|.*'$NFS_DATASET/replay/$NFS_TIMEPERIOD'\.||' -e 's|\.ops||' \
> /tmp/nfs-client-names.txt
NCLIENTS=$(cat /tmp/nfs-client-names.txt | wc -l)
if [[ $NCLIENTS -eq 0 ]]; then
  echo 'No client trace matching activity count' $NFS_MIN - $NFS_MAX 'is found.'
  exit 1
fi
echo $NCLIENTS 'clients with activity count' $NFS_MIN - $NFS_MAX 'selected.'

if [[ -n $NFS_FIND_ONLY ]]; then
  echo '$NFS_FIND_ONLY is set, stopping here.'
  exit 0
fi

# collect node names
if [[ -z $NFS_TOPO ]]; then
  echo '$NFS_TOPO is unspecified.'
  exit 2
fi

python2 $R/mn/topo_hosts.py --topo=$NFS_TOPO --endhost > /tmp/nfs-nodes.txt
if [[ $? -ne 0 ]]; then
  echo '$NFS_TOPO is invalid.'
  exit 2
fi
NNODES=$(cat /tmp/nfs-nodes.txt | wc -l)
echo $NNODES emulated nodes created.

NHOSTS=$((NSERVERS+NCLIENTS))
if [[ $NNODES -lt $NHOSTS ]]; then
  echo 'More nodes are needed on the topology.'
  exit 2
fi

if [[ -z $NFS_SERVERS ]]; then
  echo '$NFS_SERVERS unset, use initial nodes as servers.'
  head -$NSERVERS /tmp/nfs-nodes.txt > /tmp/nfs-server-nodes.txt
  tail -n+$((NSERVERS+1)) /tmp/nfs-nodes.txt | head -$NCLIENTS > /tmp/nfs-client-nodes.txt
else
  echo 'Use specified nodes as servers.'
  > /tmp/nfs-server-nodes.txt
  cp /tmp/nfs-nodes.txt /tmp/nfs-client-nodes.txt
  for S in $(cat /tmp/nfs-server-names.txt); do
    NFS_SERVER0=$(echo $NFS_SERVERS | cut -d':' -f1)
    NFS_SERVERS=$(echo $NFS_SERVERS | cut -d':' -f2-)
    if [[ -z $NFS_SERVER0 ]]; then
      echo '$NFS_SERVERS has too few server nodes.'
      exit 2
    fi
    if ! grep '^'$NFS_SERVER0'$' /tmp/nfs-client-nodes.txt >/dev/null; then
      echo 'Server node '$NFS_SERVER0' does not exist on topology or is used twice.'
      exit 2
    fi
    echo $NFS_SERVER0 >> /tmp/nfs-server-nodes.txt
    sed '/^'$NFS_SERVER0'$/ d' -i /tmp/nfs-client-nodes.txt
  done
fi

# assign node to hosts
echo 'Assign nodes for '$NSERVERS' servers and '$NCLIENTS' clients.'
paste /tmp/nfs-server-names.txt /tmp/nfs-server-nodes.txt | head -n$NCLIENTS > /tmp/nfs-server-alloc.txt
paste /tmp/nfs-client-names.txt /tmp/nfs-client-nodes.txt | head -n$NCLIENTS > /tmp/nfs-client-alloc.txt

# make traffic configuration
echo -n nfs > /tmp/nfs-traffic.txt
awk '{ printf ",s:%s:%s%s/all.paths", $2, "'$NFS_DATASET/'", $1 }' /tmp/nfs-server-alloc.txt >> /tmp/nfs-traffic.txt
awk '{ printf ",c:%s:%s:%s.%s.ops", $2, $1, "'$NFS_DATASET/replay/$NFS_TIMEPERIOD'", $1 }' /tmp/nfs-client-alloc.txt >> /tmp/nfs-traffic.txt

echo 'Traffic configuration has been generated.'
echo '--traffic $(cat /tmp/nfs-traffic.txt)'
