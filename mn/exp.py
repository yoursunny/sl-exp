#!/usr/bin/python2
"""Run SlStrategy experiment."""

import atexit
import datetime
import functools
import time
from sched import scheduler

from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.link import TCLink

from mnndn.ndn import NdnHost,Forwarder,Routing
from mnndn.topo import assignIps
from mnndn.tracer import NdnDump,TcpDump
from mnndn.net import LinkFailure

from exp_topo import makeTopo, addGateway
from exp_traffic import makeTraffic

SCHEMES = {
  'shortest': {
    'strategy': '/localhost/nfd/strategy/best-route/%FD%01',
    'routing': 'static',
    'logging': {
      'DEBUG': ['Forwarder']
    },
  },
  'bcast': {
    'strategy': '/localhost/nfd/strategy/multicast',
    'routing': 'flood',
    'logging': {
      'DEBUG': ['Forwarder']
    },
  },
  'ether': {
    'strategy': '/localhost/nfd/strategy/ether',
    'enable-ether-helper': True,
    'logging': {
      'DEBUG': ['Forwarder', 'EtherStrategy', 'EtherFib']
    },
  },
  'ronr': {
    'strategy': '/localhost/nfd/strategy/ronr',
    'logging': {
      'DEBUG': ['Forwarder', 'RonrStrategy', 'RonrFib']
    },
  },
  'sl': {
    'strategy': '/localhost/nfd/strategy/sl',
    'logging': {
      'DEBUG': ['Forwarder', 'SlStrategy', 'SlFib']
    },
  },
}

def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description='Run SlStrategy experiment.')
    parser.add_argument('--scheme', choices=SCHEMES.keys(), required=True,
                        help='forwarding scheme')
    parser.add_argument('--topo', required=True,
                        help='topology configuration string')
    parser.add_argument('--gateway',
                        help='add external gateway at specified node')
    parser.add_argument('--traffic', action='append', required=True,
                        help='traffic configuration string')
    parser.add_argument('--cscapacity', type=int, default=4096,
                        help='CS capacity')
    parser.add_argument('--env', action='append',
                        help='environ on hosts')
    parser.add_argument('--duration', type=int, default=60,
                        help='duration of emulation')
    parser.add_argument('--linkfail', action='append',
                        help='link failure configuration string')
    # TODO mobility
    parser.add_argument('--ether', action='append',
                        help='EtherStrategy command string')
    parser.add_argument('--intfqueue', type=int,
                        help='network interface queue length')
    parser.add_argument('--intfdelay', type=int,
                        help='network interface delay in millis')
    parser.add_argument('--ndndump', action='store_true',
                        help='enable ndndump')
    parser.add_argument('--tcpdump', action='store_true',
                        help='enable tcpdump')
    args = parser.parse_args()

    return args

def makeRouting(proto):
    if proto is None:
        return {
          'ctor': None
        }
    elif proto == 'static':
        from mnndn.ndn import StaticRouting
        def start(host, net):
            host.getRout().start(net)
        return {
          'ctor': StaticRouting,
          'start': start,
          'wait-for-convergence': False,
        }
    elif proto == 'flood':
        from mnndn.ndn import FloodRouting
        def start(host, net):
            host.getRout().start()
        return {
          'ctor': FloodRouting,
          'start': start,
          'wait-for-convergence': False,
        }
    elif proto == 'nlsr':
        from mnndn.ndn import NlsrRouting
        def start(host, net):
            host.getRout().start()
        return {
          'ctor': NlsrRouting,
          'start': start,
        }
    else:
        raise KeyError('unknown routing protocol')

def addDefaultRoute(host):
    fw = host.getFw()

    for intf in host.intfList():
        link = intf.link
        if link is None:
            continue

        face = None
        if link.intf1.node == host:
            face = fw.addFace(intf, link.intf2)
        elif link.intf2.node == host:
            face = fw.addFace(intf, link.intf1)

        fw.addRoute(face, '/')

def run(args):
    scheme = SCHEMES[args.scheme]
    expRouting = makeRouting(scheme.get('routing', None))
    needEndhostDefaultRoutes = scheme.get('endhost-default-routes', False)

    expTopo = makeTopo(*args.topo.split(','))
    if args.gateway is not None:
        addGateway(expTopo, args.gateway)
    topo = expTopo['topo']
    hostNames = topo.hosts()
    endhostNames = expTopo['endhosts']
    switchNames = sorted(set(hostNames) - set(endhostNames))

    traffics = [ makeTraffic(*trafficArg.split(',')) for trafficArg in args.traffic ]

    hostEnv = dict()
    if args.env is not None:
        hostEnv.update([ item.split('=', 1) for item in args.env ])

    net = Mininet(topo, link=TCLink, controller=None,
                  host=functools.partial(NdnHost, rout=expRouting['ctor'], env=hostEnv))
    net.start()
    atexit.register(net.stop)
    assignIps(net)

    if args.intfqueue:
        print 'Set network interface queue length.'
        map(lambda intf:intf.config(max_queue_size=args.intfqueue),
            [ intf for link in net.links for intf in (link.intf1, link.intf2) ])

    if args.intfdelay:
        print 'Set network interface delay.'
        map(lambda intf:intf.config(delay=('%fms' % args.intfdelay)),
            [ intf for link in net.links for intf in (link.intf1, link.intf2) ])

    if args.ndndump:
        print 'Start ndndump.'
        ndndumps = [ NdnDump(link) for link in net.links ]
        for ndndump in ndndumps:
            ndndump.start()
    if args.tcpdump:
        print 'Start tcpdump.'
        tcpdumps = [ TcpDump(link) for link in net.links ]
        for tcpdump in tcpdumps:
            tcpdump.start()

    print 'Start forwarding.'
    for hostName in hostNames:
        fw = net[hostName].getFw()
        fw.csCapacity = args.cscapacity
        for logLevel, logModules in scheme['logging'].iteritems():
            fw.setLog(logLevel, *logModules)
        fw.setStrategy('/', scheme['strategy'])
        fw.hasUdpMcast = False
        fw.wantCompressLog = True
        fw.start()
    time.sleep(5)
    Forwarder.connectPeers(net)

    if expRouting['ctor'] is not None:
        print 'Start routing.'
        for hostName in hostNames:
            expRouting['start'](net[hostName], net)

        print 'Advertise routes.'
        for hostName in hostNames:
            net[hostName].getRout().advertise('/' + hostName)

        if expRouting.get('wait-for-convergence', True):
            print 'Wait for routing convergence.'
            convergeTime = Routing.waitForConverge(net)
            if convergeTime is False:
                print 'Routing is not converged.'
                exit(1)
            print 'Routing is converged in %d seconds at %d.' % (convergeTime, time.time())

    if needEndhostDefaultRoutes:
        print 'Add default routes on end hosts.'
        for endhostName in endhostNames:
            addDefaultRoute(net[endhostName])

    sched = scheduler(time.time, time.sleep)

    lf = LinkFailure(net, sched)
    if args.linkfail is not None:
        print 'Schedule link failures.'
        for lfCmd in args.linkfail:
            lf.parseCmd(lfCmd)

    if args.ether is not None:
        if scheme.get('enable-ether-helper', False):
            print 'Process EtherStrategy commands.'
            from ether_helper import EtherHelper
            etherHelper = EtherHelper(net, sched, lf)
            for etherArg in args.ether:
                etherHelper.parseArg(etherArg)
        else:
            print 'Forwarding scheme does not support EtherStrategy commands.'
            return

    print 'Start traffic.'
    for traffic in traffics:
        traffic.start(net, sched)

    startDt = datetime.datetime.now()
    endDt = startDt + datetime.timedelta(seconds=args.duration)
    print 'Run experiment, starting at %s, estimated ending at %s.' % (startDt, endDt)
    sched.enter(args.duration, 0, lambda:0, ())
    sched.run()

    print 'Stop traffic.'
    for traffic in traffics:
        traffic.stop(net, sched)
    sched.run()
    time.sleep(1)

if __name__ == '__main__':
    setLogLevel('info')
    args = parseCommandLine()
    run(args)
