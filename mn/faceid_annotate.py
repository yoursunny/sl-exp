#!/usr/bin/python2
"""Annotate FaceIds in NFD logs with more information."""

import re

def listHosts(toppath):
    """Collect hostnames with NFD logs.
       toppath: top-level result path
       Returns host=>(nfd.log path,nfd.log.annotated path,whether xz'ed)"""
    import os, stat
    r = {}
    for host in os.listdir(toppath):
        if not stat.S_ISDIR(os.lstat('%s/%s' % (toppath, host)).st_mode):
            continue
        if os.access('%s/%s/nfd.log' % (toppath, host), os.F_OK):
            r[host] = ('%s/%s/nfd.log' % (toppath, host),
                       '%s/%s/nfd.log.annotated' % (toppath, host),
                       False)
        elif os.access('%s/%s/nfd.log.xz' % (toppath, host), os.F_OK):
            r[host] = ('%s/%s/nfd.log.xz' % (toppath, host),
                       '%s/%s/nfd.log.annotated.xz' % (toppath, host),
                       True)
    return r

def collectFaceTable(inFile):
    """Collect FaceTable from NFD log.
       inFile: opened NFD log
       Returns id=>(local,remote)"""
    faces = {}
    for line in inFile:
        tokens = line.strip().split(' ')
        if len(tokens) == 8 and tokens[2] == '[FaceTable]' and tokens[3] == 'Added':
            faces[int(tokens[5][3:])] = (tokens[7][6:], tokens[6][7:])
    return faces

RE_EXTRACT_IP = re.compile('(?:tcp|udp)4://([0-9.]+):6363') # support IPv4 only

def extractIp(faceUri):
    """Extract IP address from FaceUri.
       Returns IP address, or None."""
    m = RE_EXTRACT_IP.match(faceUri)
    if m is not None:
        return m.group(1)
    return None

RE_ANNOTATE = re.compile('(?:from|to|face|nexthop)[s]?=([0-9,]+)')

def annotateFile(inFile, outFile, faces, ipHosts):
    """Annotate a NFD log file.
       inFile: opened NFD log
       outFile: opened NFD log after annotation
       faces: FaceTable of this host
       ipHosts: IP=>host"""

    def annotateFace(faceid):
        if faceid == '':
            return ''
        faceid = int(faceid)
        local, remote = faces.get(faceid, (None, None))
        if remote is None:
            return str(faceid)
        if local[0:5] == 'unix:':
            return 'app%d' % faceid
        remoteIp = extractIp(remote)
        if remoteIp is not None:
            return ipHosts.get(remoteIp, remote)
        return remote

    def repl(m):
        faceAnnotation = [ annotateFace(faceid) for faceid in m.group(1).split(',') ]
        return m.group(0)[:(m.start(1)-m.start(0))] + ','.join(faceAnnotation)

    for line in inFile:
        line = RE_ANNOTATE.sub(repl, line)
        outFile.write(line)

def run():
    from sys import argv
    toppath = argv[1] if len(argv) > 1 else '/tmp/mnndn'
    hosts = listHosts(toppath)
    hostList = sorted(hosts)

    def openLog(host):
        (logpath, outpath, isCompressed) = hosts[host]
        if isCompressed:
            import subprocess
            xzcat = subprocess.Popen(['xzcat', logpath], stdout=subprocess.PIPE)
            return xzcat.stdout
        else:
            return open(logpath, 'r')

    def openOutput(host):
        (logpath, outpath, isCompressed) = hosts[host]
        if isCompressed:
            import subprocess
            xz = subprocess.Popen('xz', stdin=subprocess.PIPE,
                                  stdout=open(outpath, 'w'))
            return xz.stdin
        else:
            return open(outpath, 'w')

    faceTables = {} # host=>FaceTable
    for host in hosts:
        inFile = openLog(host)
        faceTables[host] = collectFaceTable(inFile)
        inFile.close()

    ipHosts = {} # IP=>host
    for host, faces in faceTables.iteritems():
        for local,remote in faces.itervalues():
            ip = extractIp(local)
            if ip is not None:
                ipHosts[ip] = host

    for host in hosts:
        inFile = openLog(host)
        outFile = openOutput(host)
        annotateFile(inFile, outFile, faceTables[host], ipHosts)
        inFile.close()
        outFile.close()

if __name__ == '__main__':
    run()
