class @FloodReach
  constructor: (@net, pktFilter=true) ->
    @nodeCounters = {}
    @portCounters = {}
    for id, node of net.topo.nodes when node.properties.type != 'repeater'
      @nodeCounters[id] = 0
      for link in net.topo.listLinksFrom id
        linkKey = Topo.makeLinkKey link
        @portCounters["#{id}:#{linkKey}"] = 0

    if pktFilter == true
      @pktFilter = -> true
    else if typeof pktFilter == 'string'
      @pktFilter = (pkt) -> pkt instanceof Interest and pkt.name == pktFilter
    else if typeof pktFilter == 'function'
      @pktFilter = pktFilter
    else
      throw Error 'invalid pktFilter'

    net.onNode 'rx', @nodeRx

  nodeRx: (node, pkt, link, from) =>
    if not @pktFilter pkt
      return
    ++@nodeCounters[node.id]
    if link == Node.LOCAL_LINK
      return
    linkKey = Topo.makeLinkKey link
    ++@portCounters["#{node.id}:#{linkKey}"]

  listUnreach: ->
    # return node ids that did not receive the packet
    return (id for id, cnt of @nodeCounters when cnt == 0)

  getMaxOverhead: ->
    return Math.max.apply(null, Object.values @portCounters)

  listExceedOverhead: ->
    # return ports that receive more packets than number of L2 peers
    exceeds = []
    for id, node of @net.topo.nodes
      for {link: link, peers: peers} in @net.topo.listL2LinksFrom id
        linkKey = Topo.makeLinkKey link
        cnt = @portCounters["#{id}:#{linkKey}"]
        if cnt > peers.length
          exceeds.push {node: id, link: linkKey, rxCount: cnt, nPeers: peers.length}
    return exceeds
