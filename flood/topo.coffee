class @Topo
  constructor: ->
    @nodes = {}
    @links = {}

  toNetJson: ->
    JSON.stringify {
      type: 'NetworkGraph'
      protocol: 'static'
      version: null
      metric: null
      nodes: Object.values @nodes
      links: Object.values @links
    }, null, '  '

  getNode: (id) ->
    return @nodes[id]

  addNode: (node) ->
    if not node.id
      throw Error 'node.id is missing or empty'
    if @getNode node.id
      throw Error 'node.id already exists'
    if not node.label?
      node.label = node.id
    @nodes[node.id] = node
    return null

  removeNode: (id) ->
    if not @nodes[id]?
      return false
    delete @nodes[id]
    for linkKey, link of @links
      if link.source == id or link.target == id
        delete @links[linkKey]
    return true

  getLink: (id1, id2) ->
    if id2?
      linkKey = Topo.makeLinkKey id1, id2
    else
      linkKey = id1
    return @links[linkKey]

  addLink: (link) ->
    if link.source == link.target
      throw Error 'link.source and link.target are the same'
    if @getLink link.source, link.target
      throw Error 'link already exists'
    if not @getNode link.source or not @getNode link.target
      throw Error 'source or target node does not exist'
    linkKey = Topo.makeLinkKey link.source, link.target
    @links[linkKey] = link
    return null

  removeLink: (id1, id2) ->
    linkKey = Topo.makeLinkKey id1, id2
    if not @links[linkKey]?
      return false
    delete @links[linkKey]
    return true

  listLinksFrom: (id) ->
    return (link for linkKey, link of @links when link.source == id or link.target == id)

  getL1Peer: (id, link) ->
    return @getNode link[Topo.farend(id, link)]

  listL2LinksFrom: (id) ->
    return ({ link: link, cost: link.cost, peers: @listL2PeersOn id, link } for link in @listLinksFrom id)

  listL2PeersOn: (id, link) ->
    l1peer = @getL1Peer id, link
    if l1peer.properties.type != 'repeater'
      return [{ peer: l1peer.id, link: link }]
    l2peers = []
    for link2 in @listLinksFrom l1peer.id when link2 != link
      l2peers = l2peers.concat @listL2PeersOn l1peer.id, link2
    return l2peers

Topo.makeLinkKey = (id1, id2) ->
  if arguments.length == 1
    link = id1
    id1 = link.source
    id2 = link.target
  if id1 <= id2
    return "#{id1}_#{id2}"
  else
    return "#{id2}_#{id1}"

Topo.nearend = (id, link) ->
  return if link.source == id then 'source' else 'target'
Topo.farend = (id, link) ->
  return if link.source == id then 'target' else 'source'

Topo.fromNetJson = (json) ->
  if typeof json == 'string'
    json = JSON.parse json
  topo = new Topo
  topo.addNode node for node in [].concat json.nodes
  topo.addLink link for link in [].concat json.links
  return topo
