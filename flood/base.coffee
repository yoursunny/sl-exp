Object.values ?= (obj) ->
  return (obj[k] for k in Object.keys(obj))

@newGuid = ->
  newHexString = (len) ->
    ('0123456789ABCDEF'.charAt(Math.floor(Math.random()*16)) for [0...len]).join('')
  return (newHexString len for len in [8, 4, 4, 4, 12]).join('-')

# https://gist.github.com/contra/2759355
class @EventEmitter
  constructor: ->
    @events = {}

  emit: (event, args...) ->
    return false unless @events[event]
    listener args... for listener in @events[event]
    return true

  addListener: (event, listener) ->
    @emit 'newListener', event, listener
    (@events[event]?=[]).push listener
    return @

  on: @::addListener

  once: (event, listener) ->
    fn = =>
      @removeListener event, fn
      listener arguments...
    @on event, fn
    return @

  removeListener: (event, listener) ->
    return @ unless @events[event]
    @events[event] = (l for l in @events[event] when l isnt listener)
    return @

  removeAllListeners: (event) ->
    delete @events[event]
    return @

d3.selection.prototype.hide ?= ->
  this.style 'display', 'none'
d3.selection.prototype.show ?= ->
  this.style 'display', ''
