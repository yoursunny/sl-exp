class Scheduler
  constructor: ->
    @reset()

  reset: ->
    @now = 0
    @evts = []

  schedule: (after, f) ->
    evt =
      t: @now + after
      f: f
      isCancelled: false
    i = @evts.findIndex (evt1) -> evt1.t > evt.t
    i = if i >= 0 then i else @evts.length
    @evts.splice i, 0, evt
    return evt

  cancel: (evt) ->
    evt.isCancelled = true

  run: (cb) ->
    while @evts.length > 0
      evt = @evts.shift()
      @now = evt.t
      if not evt.isCancelled
        evt.f()
    cb?()

@sched = new Scheduler
