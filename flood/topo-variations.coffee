class @TopoVariations
  constructor: (@topo) ->

  listSwitches: ->
    return (id for id, node of @topo.nodes when node.properties.type in ['E-switch', 'N-switch'])

  getNSwitchList: ->
    return (id for id, node of @topo.nodes when node.properties.type == 'N-switch')

  setNSwitchList: (nSwitchList) ->
    for id, node of @topo.nodes when node.properties.type in ['E-switch', 'N-switch']
      node.properties.type = if id in nSwitchList then 'N-switch' else 'E-switch'
    return null

  genNSwitchLists: (switches=@listSwitches()) ->
    if switches.length == 0
      return [[]]
    head = switches[0]
    tail = switches.slice(1)
    excludeLists = @genNSwitchLists tail
    includeLists = ([head].concat(list) for list in excludeLists)
    return excludeLists.concat(includeLists)

  getBridgeIdAssignment: ->
    assign = {}
    for id, node of @topo.nodes when node.properties.type in ['E-switch', 'N-switch']
      assign[id] = node.properties.bridgeId
    return assign

  setBridgeIdAssignment: (assign) ->
    for id, node of @topo.nodes when node.properties.type in ['E-switch', 'N-switch']
      node.properties.bridgeId = assign[id]
    rstp = new Rstp @topo
    rstp.converge()
    return null

  genBridgeIdAssignments: ->
    assignBridgeIds = (order) ->
      assign = {}
      bridgeId = 0
      for id in order
        assign[id] = ++bridgeId
      return assign
    return (assignBridgeIds(order) for order in @genBridgeOrders())

  genBridgeOrders: (bridges=@listSwitches()) ->
    if bridges.length == 0
      return [[]]
    return [].concat (bridges.map (head, i) =>
      tail = [].concat(bridges)
      tail.splice(i, 1)
      return ([head].concat(tailOrder) for tailOrder in @genBridgeOrders tail)
    )...

  listOrigins: (topo) ->
    return (id for id, node of topo.nodes when node.properties.type in ['N-host', 'N-switch'])

  countCases: (nSwitchLists, bridgeIdAssignments) ->
    nNHosts = (id for id, node of @topo.nodes when node.properties.type == 'N-host').length
    return bridgeIdAssignments.length * (nSwitchLists.reduce ((sum, nSwitchList) -> sum + nNHosts + nSwitchList.length), 0)
