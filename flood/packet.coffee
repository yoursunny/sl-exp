class @Packet
  constructor: ->
    @hopCount = 0

  clone: ->
    copy = new @__proto__.constructor()
    return Object.assign copy, @

  toString: ->
    return 'packet'

class @Interest extends Packet
  constructor: (@name, @nonce) ->
    @nonce ?= Interest.makeNonce()

  toString: ->
    return "I~#{@name}~#{@nonce}"

Interest.makeNonce = -> Math.floor 0xffffffff * Math.random()

class @Data extends Packet
  constructor: (@name) ->

  toString: ->
    return "D~#{@name}"

class @Nack extends Packet
  constructor: (@interest, @reason) ->

  toString: ->
    return "N~#{@interest.name}~#{@interest.nonce}~#{@reason}"

@Nack.CONGESTION = 'CONGESTION'
@Nack.DUPLICATE = 'DUPLICATE'
@Nack.NOROUTE = 'NOROUTE'
