class TopoVis extends EventEmitter
  constructor: (el, options = {}) ->
    super()
    @topo = new Topo
    @width = options.width ? 500
    @height = options.height ? 500
    @nodeSize = options.nodeSize ? 20
    @portSize = options.portSize ? @nodeSize * 0.2
    @vis = d3.select el
             .append 'svg'
             .attr 'width', @width
             .attr 'height', @height
             .on 'click.TopoVis', => @emit 'clickCanvas'
    @gLinks = @vis.append 'g'
    @gNodes = @vis.append 'g'
    @gPorts = @vis.append 'g'

  calcX: (x) -> x / 1000 * @width
  calcY: (y) -> y / 1000 * @height
  calcCornerR: (node) -> if /host/.test(node.properties.type) then 0 else @nodeSize * 0.5
  calcFill: (node) ->
    switch node.properties.type
      when 'N-switch' then '#ff9900'
      when 'E-switch' then '#00ff00'
      when 'N-host' then '#ff9900'
      when 'E-host' then '#00ff00'
      when 'repeater' then '#999999'

  draw: (topo) ->
    @topo = topo ? @topo
    @drawNodes()
    @drawLinks()
    @drawPorts()
    return null

  drawNodes: ->
    sel = @gNodes.selectAll '.node'
                 .data Object.values @topo.nodes
    g = sel.enter()
      .append 'g'
      .classed 'node', true
      .on 'click.TopoVis', () =>
        id = d3.select d3.event.target.parentElement
               .attr 'data-id'
        if node = @topo.getNode id
          @emit 'clickNode', node
        d3.event.stopPropagation()
    g.append 'rect'
      .attr 'stroke', '#000000'
      .attr 'width', @nodeSize
      .attr 'height', @nodeSize
    g.append 'text'
      .attr 'font-size', @nodeSize * 0.6

    sel.attr 'data-id', (node) -> node.id

    sel.select 'rect'
      .attr 'x', (node) => @calcX(node.properties.x) - @nodeSize * 0.5
      .attr 'y', (node) => @calcY(node.properties.y) - @nodeSize * 0.5
      .attr 'rx', (node) => @calcCornerR(node)
      .attr 'ry', (node) => @calcCornerR(node)
      .attr 'fill', (node) => @calcFill(node)

    sel.select 'text'
      .attr 'x', (node) => @calcX(node.properties.x) - @nodeSize * 0.3
      .attr 'y', (node) => @calcY(node.properties.y) + @nodeSize * 0.3
      .text (node) => node.label

    sel.exit()
      .remove()
    return null

  drawLinks: ->
    sel = @gLinks.selectAll '.link'
                 .data Object.values @topo.links
    sel.enter()
      .append 'svg:line'
      .classed 'link', true
      .style 'stroke', '#000000'
    sel
      .attr 'x1', (link) => @calcX(@topo.getNode(link.source).properties.x)
      .attr 'y1', (link) => @calcY(@topo.getNode(link.source).properties.y)
      .attr 'x2', (link) => @calcX(@topo.getNode(link.target).properties.x)
      .attr 'y2', (link) => @calcY(@topo.getNode(link.target).properties.y)
    sel.exit()
      .remove()
    return null

  calcPortXY: ({link: linkKey, end: end}) ->
    link = @topo.getLink linkKey
    nearNode = @topo.getNode link[end]
    farNode = @topo.getNode link[Topo.farend(nearNode.id, link)]
    nearX = @calcX(nearNode.properties.x)
    nearY = @calcY(nearNode.properties.y)
    dx = @calcX(farNode.properties.x) - nearX
    dy = @calcY(farNode.properties.y) - nearY
    r = Math.sqrt dx * dx + dy * dy
    ratio = (@nodeSize + @portSize) / 2 / r
    return {x: nearX + dx * ratio, y: nearY + dy * ratio}

  calcPortX: (port) -> @calcPortXY(port).x
  calcPortY: (port) -> @calcPortXY(port).y

  drawPorts: ->
    ports = {}
    ports[Rstp.DESIGNATED] = []
    ports[Rstp.ROOT] = []
    ports[Rstp.BACKUP] = []
    for linkKey, link of @topo.links
      ports[link.properties?.sourceRstpRole]?.push {link: linkKey, end: 'source'}
      ports[link.properties?.targetRstpRole]?.push {link: linkKey, end: 'target'}

    sel = @gPorts.selectAll '.port-designated'
                 .data ports[Rstp.DESIGNATED]
    sel.enter()
      .append 'svg:circle'
      .classed 'port-designated', true
      .attr 'r', @portSize
      .attr 'fill', '#000000'
    sel
      .attr 'cx', (port) => @calcPortX(port)
      .attr 'cy', (port) => @calcPortY(port)
    sel.exit()
      .remove()

    sel = @gPorts.selectAll '.port-root'
                 .data ports[Rstp.ROOT]
    sel.enter()
      .append 'svg:circle'
      .classed 'port-root', true
      .attr 'r', @portSize
      .attr 'stroke', '#000000'
      .attr 'stroke-width', @portSize * 0.2
      .attr 'fill', 'transparent'
    sel
      .attr 'cx', (port) => @calcPortX(port)
      .attr 'cy', (port) => @calcPortY(port)
    sel.exit()
      .remove()

    makeBackupPortSvgPath = (port) =>
      {x: x, y: y} = @calcPortXY port
      return "M #{x} #{y-@portSize} l #{@portSize} #{@portSize} l #{-@portSize} #{@portSize}"
    sel = @gPorts.selectAll '.port-backup'
                 .data ports[Rstp.BACKUP]
    sel.enter()
      .append 'svg:path'
      .classed 'port-backup', true
      .attr 'stroke', '#000000'
      .attr 'stroke-width', @portSize * 0.4
      .attr 'fill', 'transparent'
    sel
      .attr 'd', makeBackupPortSvgPath
    sel.exit()
      .remove()

    return null

  getNodeEl: (id) ->
    return d3.select ".node[data-id='#{id}']"

@TopoVis = TopoVis
