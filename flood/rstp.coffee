class @Rstp
  constructor: (@topo) ->

  listBridges: ->
    @bridges ?= (node for id, node of @topo.nodes when node.properties.type in ['E-switch', 'N-switch'])
    return [].concat @bridges

  clearBridgeIds: ->
    for bridge in @listBridges()
      delete bridge.properties.bridgeId
    return null

  setBridgeId: (id, bridgeId) ->
    @topo.getNode(id)?.properties.bridgeId = bridgeId
    return null

  countBridgeIdUnassigned: ->
    return (bridge for bridge in @listBridges() when not bridge.properties.bridgeId?).length

  randomizeBridgeIds: (clear=true) ->
    if clear then @clearBridgeIds()
    bridges = @listBridges()
    bridges.sort -> Math.random() - 0.5

    bridgeId = 0
    bridgeId = Math.max(bridgeId, bridge.properties.bridgeId ? 0) for bridge in bridges

    for bridge in bridges when not bridge.properties.bridgeId?
      bridge.properties.bridgeId = ++bridgeId
    return null

  renameBridges: ->
    for bridge in @listBridges()
      bridge.label = bridge.properties.type.charAt(0) + (bridge.properties.bridgeId ? '?')

  findRoot: ->
    minBridgeId = Infinity
    rootBridge = null
    for bridge in @listBridges()
      if bridge.properties.bridgeId < minBridgeId
        minBridgeId = bridge.properties.bridgeId
        rootBridge = bridge
    return rootBridge

  converge: ->
    bridge.properties.rstpRootCost = Infinity for bridge in @listBridges()
    for linkKey, link of @topo.links
      link.properties ?= {}
      delete link.properties['sourceRstpRole']
      delete link.properties['targetRstpRole']

    root = @findRoot()
    if not root?
      return
    root.properties.rstpRootCost = 0

    compareBridge = (a, b) ->
      if a.properties.rstpRootCost != b.properties.rstpRootCost
        return a.properties.rstpRootCost - b.properties.rstpRootCost
      return a.properties.bridgeId - b.properties.bridgeId

    unconverged = @listBridges()
    rootPorts = {}
    until unconverged.length == 0
      unconverged.sort compareBridge
      u = unconverged.shift()
      for {cost: cost, peers: l2peers} in @topo.listL2LinksFrom u.id
        alt = u.properties.rstpRootCost + cost
        for {peer: l2peerId, link: rootLink} in l2peers
          l2peer = @topo.getNode l2peerId
          if not l2peer in unconverged
            continue
          if alt < l2peer.properties.rstpRootCost
            l2peer.properties.rstpRootCost = alt
            rootPorts[l2peerId] = rootLink

    bridges = @listBridges()
    bridges.sort compareBridge
    for bridge in bridges
      for {link: link, peers: l2peers} in @topo.listL2LinksFrom bridge.id
        designated = null
        designated = l2peerId for {peer: l2peerId, link: peerLink} in l2peers when peerLink.properties[Topo.nearend(l2peerId, peerLink) + 'RstpRole'] == Rstp.DESIGNATED
        link.properties[Topo.nearend(bridge.id, link) + 'RstpRole'] =
          if designated?
            if designated == bridge.id
              Rstp.BACKUP
            else if rootPorts[bridge.id] == link
              Rstp.ROOT
            else
              Rstp.ALTERNATE
          else
            Rstp.DESIGNATED
    return null

Rstp.DESIGNATED = 'D'
Rstp.ROOT = 'R'
Rstp.BACKUP = 'B'
Rstp.ALTERNATE = 'A'

Rstp.isDiscarding = (role) -> role == Rstp.BACKUP or role == Rstp.ALTERNATE
