class @TopoDesigner
  constructor: (@topovis) ->

  uncalcX: (x) -> x / @topovis.width * 1000
  uncalcY: (y) -> y / @topovis.height * 1000

  moveNode: (node, cb) ->
    drag = d3.behavior.drag()
    g = @topovis.vis.select "[data-id='#{node.id}']"
      .call drag
    rect = g.select 'rect'
      .attr 'stroke-dasharray', '5,5'
    text = g.select 'text'

    drag.on 'drag', (node) =>
      evt = d3.event.sourceEvent
      node.properties.x = @uncalcX evt.offsetX
      node.properties.y = @uncalcY evt.offsetY
      rect.attr 'x', evt.offsetX - @topovis.nodeSize * 0.5
          .attr 'y', evt.offsetY - @topovis.nodeSize * 0.5
      text.attr 'x', evt.offsetX - @topovis.nodeSize * 0.3
          .attr 'y', evt.offsetY + @topovis.nodeSize * 0.3
    drag.on 'dragend', () =>
      g.on '.drag', null
      rect.attr 'stroke-dasharray', ''
      @topovis.draw()
      cb null, node

  makeGrid: (options) ->
    rows = options.rows ? 3
    cols = options.cols ? 3
    endHosts = options.endHosts ? 0
    # number of end hosts connected on switch through a repeater
    linkHosts = options.linkHosts ? 0
    # number of hosts connected on link between grid switches

    hskip = 1000 / rows
    vskip = 1000 / cols
    hoffset = hskip * 0.5
    voffset = vskip * 0.5
    placeSwitch = (row, col) ->
      return {
        x: row * hskip + hoffset
        y: col * vskip + voffset
      }
    placeEndRepeater = (row, col) ->
      {x: x, y: y} = placeSwitch row, col
      return {
        x: x - hskip * 0.2
        y: y - vskip * 0.2
      }
    placeEndHost = (row, col, h) ->
      {x: x, y: y} = placeEndRepeater row, col
      if endHosts == 1
        return {x: x, y: y}
      return {
        x: x - h * hskip * 0.2
        y: y - vskip * 0.25
      }
    placeLinkRepeater = (row1, col1, row2, col2) ->
      {x: x1, y: y1} = placeSwitch row1, col1
      {x: x2, y: y2} = placeSwitch row2, col2
      return {
        x: (x1 + x2) / 2
        y: (y1 + y2) / 2
      }
    placeLinkHost = (row1, col1, row2, col2, h) ->
      {x: x, y: y} = placeLinkRepeater row1, col1, row2, col2
      if row1 == row2
        return {
          x: x + hskip * 0.25
          y: y + h * vskip * 0.2
        }
      else
        return {
          x: x + h * hskip * 0.2
          y: y + vskip * 0.25
        }

    topo = new Topo
    addNode = (id, type, {x: x, y: y}) ->
      topo.addNode {
        id: id
        properties:
          type: type
          x: x
          y: y
      }
    addLink = (id1, id2) ->
      topo.addLink {
        source: id1
        target: id2
        cost: 1
      }

    for row in [0...rows]
      for col in [0...cols]
        if row > 0 and linkHosts > 0
          addNode "#{row-1}x#{col}c", 'repeater', (placeLinkRepeater row - 1, col, row, col)
          for h in [0...linkHosts]
            addNode "#{row-1}x#{col}c#{h}", 'N-host', (placeLinkHost row - 1, col, row, col, h)
            addLink "#{row-1}x#{col}c", "#{row-1}x#{col}c#{h}"

        if col > 0 and linkHosts > 0
            addNode "#{row}x#{col-1}r", 'repeater', (placeLinkRepeater row, col - 1, row, col)
            for h in [0...linkHosts]
              addNode "#{row}x#{col-1}r#{h}", 'N-host', (placeLinkHost row, col - 1, row, col, h)
              addLink "#{row}x#{col-1}r", "#{row}x#{col-1}r#{h}"

        addNode "#{row}x#{col}", 'N-switch', (placeSwitch row, col)

        if row > 0
          if linkHosts > 0
            addLink "#{row-1}x#{col}", "#{row-1}x#{col}c"
            addLink "#{row-1}x#{col}c", "#{row}x#{col}"
          else
            addLink "#{row-1}x#{col}", "#{row}x#{col}"

        if col > 0
          if linkHosts > 0
            addLink "#{row}x#{col-1}", "#{row}x#{col-1}r"
            addLink "#{row}x#{col-1}r", "#{row}x#{col}"
          else
            addLink "#{row}x#{col-1}", "#{row}x#{col}"

        if endHosts == 1
          addNode "#{row}x#{col}h0", 'N-host', (placeEndHost row, col, 0)
          addLink "#{row}x#{col}", "#{row}x#{col}h0"
        else if endHosts > 1
          addNode "#{row}x#{col}h", 'repeater', (placeEndRepeater row, col)
          addLink "#{row}x#{col}", "#{row}x#{col}h"
          for h in [0...endHosts]
            addNode "#{row}x#{col}h#{h}", 'N-host', (placeEndHost row, col, h)
            addLink "#{row}x#{col}h", "#{row}x#{col}h#{h}"

    @topovis.draw topo
