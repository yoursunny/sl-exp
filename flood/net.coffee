class @Net
  constructor: (@topo) ->
    @nodes = {}

  getNode: (id) ->
    return @nodes[id]

  addNode: (node) ->
    @nodes[node.id] = node

  build: ->
    for id, topoNode of @topo.nodes
      nodeType = switch topoNode.properties.type
        when 'E-host' then EtherHost
        when 'E-switch' then EtherSwitch
        when 'N-host' then NdnHost
        when 'N-switch' then NdnSwitch
      if nodeType?
        @addNode new nodeType this, id
    return null

  onNode: (evt, f) ->
    for id, node of @nodes
      node.on evt, f.bind(node, node)

  enableTextLog: (logFunc=console.log.bind(console)) ->
    log = (node, s) -> logFunc "#{sched.now} [#{node.label}] #{s}"
    @onNode 'rx', (node, pkt, link, from) =>
      fromLabel = if from == Node.LOCAL_FROM then 'app' else @topo.getNode(from).label
      linkLabel = if link == Node.LOCAL_LINK then 'local' else @topo.getNode(link[Topo.farend(node.id, link)]).label
      if fromLabel == linkLabel
        log node, "#{pkt.toString()} from #{fromLabel}"
      else
        log node, "#{pkt.toString()} from #{fromLabel} via #{linkLabel}"
    @onNode 'tx', (node, pkt, link, from) =>
      linkLabel = if link == Node.LOCAL_LINK then 'local' else @topo.getNode(link[Topo.farend(node.id, link)]).label
      log node, "#{pkt.toString()} onto #{linkLabel}"
    @onNode 'rstpdiscard', (node, pkt, link, from) -> log node, 'drop-RSTP-discarding'
    @onNode 'dupnonce', (node, pkt, link, from) -> log node, 'drop-duplicate-Nonce'
    @onNode 'addduplink', (node, link) =>
      linkLabel = @topo.getNode(link[Topo.farend(node.id, link)]).label
      log node, "add-dup-link #{linkLabel}"

class @Node extends EventEmitter
  constructor: (@net, @id) ->
    super arguments...
    @topo = @net.topo
    topoNode = @topo.getNode @id
    @label = topoNode.label
    @properties = topoNode.properties

  startPacket: (pkt) ->
    @processPacket pkt, Node.LOCAL_LINK, Node.LOCAL_FROM

  processPacket: (pkt, link, from) ->
    @emit 'rx', pkt, link, from

  sendPacket: (pkt, link) ->
    arrival = sched.now + link.cost
    @emit 'tx', pkt, link
    sched.schedule link.cost, =>
      for {peer: peer, link: peerLink} in @topo.listL2PeersOn @id, link
        @net.getNode peer
            .processPacket pkt, peerLink, @id

Node.LOCAL_LINK = {}
Node.LOCAL_FROM = {}

class @EtherNode extends Node
  constructor: ->
    super arguments...

  processPacket: ->
    super arguments...
    @processEtherFrame arguments...

class @EtherHost extends EtherNode
  constructor: ->
    super arguments...

  processEtherFrame: (pkt, link, from) ->
    # ignore

class @EtherSwitch extends EtherNode
  constructor: ->
    super arguments...

  processEtherFrame: (pkt, link, from) ->
    if Rstp.isDiscarding link.properties[Topo.nearend(@id, link) + 'RstpRole']
      @emit 'rstpdiscard', pkt, link, from
      return
    for olink in @topo.listLinksFrom @id when olink != link
      if not Rstp.isDiscarding olink.properties[Topo.nearend(@id, olink) + 'RstpRole']
        @sendPacket pkt, olink

class @NdnNode extends EtherNode
  NdnNode_ctor: ->
    @seenNonces = {}

  processPacket: (pkt, link, from) ->
    Node.prototype.processPacket.apply this, arguments
    switch
      when pkt instanceof Interest then @processInterest arguments...
      when pkt instanceof Data then @processData arguments...
      when pkt instanceof Nack then @processNack arguments...
      else @processEtherFrame arguments...

  hasDuplicateNonce: (pkt, link, from) ->
    nonceList = @seenNonces[pkt.name] ?= {}
    if nonceList[pkt.nonce]?
      return true
    nonceList[pkt.nonce] = true
    return false

class @NdnHost extends EtherHost
  constructor: ->
    super arguments...
    @NdnNode_ctor arguments...

  processInterest: (pkt, link, from) ->
    if @hasDuplicateNonce arguments...
      @emit 'dupnonce', pkt, link, from
      return
    if link == Node.LOCAL_LINK
      for olink in @topo.listLinksFrom @id
        @sendPacket pkt, olink

  processData: ->
    # ignore

  processNack: ->
    # ignore

class @NdnSwitch extends EtherSwitch
  constructor: ->
    super arguments...
    @NdnNode_ctor arguments...

    @ndnLinks = [] # links with no E-switch peer
    @etherLinks = [] # links with E-switch peer(s), local port is not designated port
    @designatedLinks = [] # links with E-switch peer(s), local port is designated port
    for {link: link, peers: peers} in @topo.listL2LinksFrom @id
      hasESwitch = peers.some ({peer: peer}) =>
        return @topo.getNode(peer).properties.type == 'E-switch'
      if not hasESwitch
        @ndnLinks.push link
      else if link[Topo.nearend(@id, link) + 'RstpRole'] == Rstp.DESIGNATED
        @designatedLinks.push link
      else
        @etherLinks.push link
    @dupLinks = [] # all but one link that are connected together by E-switches

  processInterest: (pkt, link, from) ->
    if pkt.lastNSwitch == @id
      unless pkt.lastNSwitchPort in @dupLinks
        @dupLinks.push link
        @emit 'addduplink', link

    if @hasDuplicateNonce arguments...
      @emit 'dupnonce', pkt, link, from
      return

    traversedDiscardingPort = (pkt.traversedDiscardingPort ? false) or Rstp.isDiscarding link[Topo.nearend(@id, link) + 'RstpRole']
    for olink in @ndnLinks when olink != link
      @forwardInterest pkt, olink, traversedDiscardingPort
    for olink in @designatedLinks when olink != link and olink not in @dupLinks
      @forwardInterest pkt, olink, traversedDiscardingPort
    unless traversedDiscardingPort
      for olink in @etherLinks when olink != link and olink not in @dupLinks
        isDiscardingPort = Rstp.isDiscarding olink[Topo.nearend(@id, olink) + 'RstpRole']
        @forwardInterest pkt, olink, traversedDiscardingPort or isDiscardingPort

  forwardInterest: (pkt, link, traversedDiscardingPort) ->
    pkt = pkt.clone()
    pkt.traversedDiscardingPort = traversedDiscardingPort
    pkt.lastNSwitch = @id
    pkt.lastNSwitchPort = link
    @sendPacket pkt, link

  processData: ->
    # ignore

  processNack: ->
    # ignore

cls.prototype[funcName] = func for funcName, func of NdnNode.prototype when funcName != 'constructor' for cls in [NdnHost, NdnSwitch]
