#!/bin/bash
R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

DIRTPL=$1

SCHEMES[0]=bcast:broadcast
SCHEMES[1]=sl:self-learning
SCHEMES[2]=ronr:RONR

for SCHEME in ${SCHEMES[@]}; do
  KEY=$(echo $SCHEME | cut -d: -f1)
  LABEL=$(echo $SCHEME | cut -d: -f2)
  DIR=$(echo $DIRTPL | sed 's/%/'$KEY'/')
  if [[ ! -d $DIR ]]; then
    echo $DIR is missing > /dev/stderr
    exit 1
  fi

  echo -n $KEY
  echo -ne '\t'
  echo -n $LABEL
  echo -ne '\t'

  HOSTNAMES=$(cd $DIR; ls -d */ | awk 'NR>1{ printf("|") } { printf("%s", substr($0, 1, length($0)-1)) }')
  xzgrep -h '\[Forwarder\] onIncoming' $DIR/*/var/log/ndn/nfd.log.annotated.xz | awk '
  BEGIN {
    nInInterests = nInData = nInNacks = 0
  }
  $5 ~ "face=(?:'$HOSTNAMES')" {
    if ($4 == "onIncomingInterest") {
      ++nInInterests
    }
    else if ($4 == "onIncomingData") {
      ++nInData
    }
    else if ($4 == "onIncomingNack") {
      ++nInNacks
    }
  }
  END {
    OFS = "\t"
    print nInInterests, nInData, nInNacks
  }
  '
done > total-traffic.tsv

gnuplot $R/analyze/total-traffic.gnuplot
