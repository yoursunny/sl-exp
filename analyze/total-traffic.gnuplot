set term pdfcairo dashed font ",12";
set out "total-traffic.pdf";

set border 3;
set key outside invert right top Left reverse samplen 2;
set xtics nomirror;
set ytics nomirror;

set xlabel "scheme";
set ylabel "total traffic";

set style data histograms;
set style histogram rowstacked;
set boxwidth 0.5;

plot "total-traffic.tsv" using 3 lt 1 lc 0 fs pattern 4 title "Interests", \
     "" using 4 lt 1 lc 1 fs pattern 1 title "Data", \
     "" using 5:xtic(2) lt 1 lc 3 fs pattern 7 title "Nacks";
