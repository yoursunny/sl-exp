set term pdfcairo dashed font ",12";
set out "fib-size.pdf";

set border 3;
set key left top Left reverse samplen 2;
set xtics nomirror;
set ytics nomirror;

set xlabel "time (s)";
set ylabel "total FIB size";

plot for [IDX=0:$NSCHEMES-1] "fib-size.tsv" index IDX using 1:2 with dots lc IDX title columnheader(1);
