#!/bin/bash
R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"

DIRTPL=$1

SCHEMES[1]=sl:self-learning:SlFib
SCHEMES[2]=ronr:RONR:RonrFib

NSCHEMES=0
for SCHEME in ${SCHEMES[@]}; do
  KEY=$(echo $SCHEME | cut -d: -f1)
  LABEL=$(echo $SCHEME | cut -d: -f2)
  FIBMODULE=$(echo $SCHEME | cut -d: -f3)
  DIR=$(echo $DIRTPL | sed 's/%/'$KEY'/')
  if [[ ! -d $DIR ]]; then
    echo $DIR is missing > /dev/stderr
    exit 1
  fi

  NSCHEMES=$((NSCHEMES+1))
  echo '"'$LABEL'"'

  pushd $DIR > /dev/null
  # 1. grep finds lines like: timestamp [SomeFib] size=n
  # 2. awk writes: nodename, timestamp, size
  # 3. sort sorts the logs by increasing timestamp
  # 4. awk adds FIB size of all nodes together as a running sum, and prints: relative timestamp, sum
  xzgrep -H -E '\['$FIBMODULE'\] size=' */var/log/ndn/nfd.log.annotated.xz | \
  awk '{print substr($1,1,index($1,"/")-1), substr($1,index($1,":")+1,length($1)), substr($4,6,length($4))}' | \
  sort -nk2 | \
  awk 'BEGIN{OFS="\t"; sum=0} NR==1{t0=$2} {sum=sum+$3-last[$1]; last[$1]=$3; print $2-t0,sum}'
  popd > /dev/null

  echo
  echo

done > fib-size.tsv

sed 's/$NSCHEMES/'$NSCHEMES'/' $R/analyze/fib-size.gnuplot | gnuplot
