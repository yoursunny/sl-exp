#!/bin/bash
# Collect traffic counters from one experiment in current directory.

PREFIX=$1

if ! find */nfd.log.annotated.xz &>/dev/null; then
  echo 'Run mn/faceid_annotate.py '$(pwd)' first.' >/dev/stderr
  exit 4
fi

(
  echo -e 'nInterests\tnData\tnNacks\tnFibPeak'

  xzgrep -E '\[Forwarder\] onIncoming.*'$PREFIX */nfd.log.annotated.xz \
  | sed -e '/ face=app/ d' -e '/ matching=/ d' \
  | cut -d' ' -f4 | awk '
    { ++cnt[$1] }
    END { printf "%d\t%d\t%d\t", 0+cnt["onIncomingInterest"], 0+cnt["onIncomingData"], 0+cnt["onIncomingNack"] }
  '

  xzgrep 'Fib] size=' */nfd.log.annotated.xz | cut -d= -f2 \
  | awk '
    BEGIN { fibPeak = 0 }
    $1 > fibPeak { fibPeak = 0+$1 }
    END { printf "%d\n", fibPeak }
  '
) > packet-counters.tsv
