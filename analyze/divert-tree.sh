#!/bin/bash
# analyze Interest diversion on a tree topology

# The experiment should run internal + external traffic on a tree topology,
# where the root of the tree is connected to a 'gateway' and then goes to 'external'.
# Interest names should start with /external for external traffic,
# and start with /<producer-name> for internal traffic.
# This script should be invoked in /tmp/mnndn after cleanup and faceid_annotate.

R="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. && pwd )"
COLPREFIX=divert-tree.column

HEIGHT=${1:-2}
DEGREE=${2:-4}

function makeHostDepthRegex {
  local DEPTH=$1
  echo -n h
  for I in $(seq $DEPTH); do echo -n .; done
}

# LAN packet counts
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingInterest face=.* interest=/external/' | grep -v 'face=\(app\|external\)' | wc -l > $COLPREFIX.A0.nLanExtInterests &
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingData face=.* data=/external/' | grep -v 'face=\(app\|external\)' | wc -l > $COLPREFIX.A1.nLanExtData &
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingNack face=.* nack=/external/' | grep -v 'face=\(app\|external\)' | wc -l > $COLPREFIX.A2.nLanExtNacks &
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingInterest face=' | grep -v 'face=\(app\|external\)\|interest=/external/' | wc -l > $COLPREFIX.A5.nLanIntInterests &
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingData face=' | grep -v 'face=\(app\|external\)\|data=/external/' | wc -l > $COLPREFIX.A6.nLanIntData &
find -name 'nfd.log.annotated.xz' ! -path './external/*' | xargs xzgrep 'onIncomingNack face=' | grep -v 'face=\(app\|external\)\|nack=/external/' | wc -l > $COLPREFIX.A7.nLanIntNacks &

# WAN packet counts
xzgrep 'onIncomingInterest face=gateway interest=/external/' external/nfd.log.annotated.xz | wc -l > $COLPREFIX.B0.nWanExtInterests &
xzgrep 'onIncomingData face=external data=/external/' gateway/nfd.log.annotated.xz | wc -l > $COLPREFIX.B1.nWanExtData &
xzgrep 'onIncomingNack face=external nack=/external/' gateway/nfd.log.annotated.xz | wc -l > $COLPREFIX.B2.nWanExtNacks &

# end-to-end success rate
grep -h lost */external.log | awk '{ nInterests += $1; nData += $4; nNacks += $6; } END { print nData / (nInterests - nNacks) }' > $COLPREFIX.E0.e2eExtSuccess &
grep -h lost */internal.log | awk '{ nInterests += $1; nData += $4; nNacks += $6; } END { print nData / (nInterests - nNacks) }' > $COLPREFIX.E1.e2eIntSuccess &

# LAN cache hits without diversion
xzgrep 'onContentStoreHit interest=/external/.* from=h$' gateway/nfd.log.annotated.xz | wc -l > $COLPREFIX.H0000.nExtNormalHitsGateway &
for DEPTH in $(seq 0 $HEIGHT); do
  if [[ $DEPTH -lt $HEIGHT ]]; then
    DOWNSTREAM=$(makeHostDepthRegex $((DEPTH+1)))'$'
  else
    DOWNSTREAM='app'
  fi
  find -regex '.*/'$(makeHostDepthRegex $DEPTH)'/nfd.log.annotated.xz' | xargs xzgrep 'onContentStoreHit interest=/external/.* from='$DOWNSTREAM | wc -l > $COLPREFIX.H$(printf %02d $DEPTH).nExtNormalHits$DEPTH &
done

# diversions
for DEPTH in $(seq 0 $((HEIGHT-1))); do
  find -regex '.*/'$(makeHostDepthRegex $DEPTH)'/nfd.log.annotated.xz' | xargs xzgrep 'divert-to='$(makeHostDepthRegex $((DEPTH+1)))'$' | grep -v ' thres=0 ' | wc -l > $COLPREFIX.K$(printf %02d $DEPTH).nDiversions$DEPTH &
  find -regex '.*/'$(makeHostDepthRegex $DEPTH)'/nfd.log.annotated.xz' | xargs xzgrep 'thres=0 .* divert-to='$(makeHostDepthRegex $((DEPTH+1)))'$' | wc -l > $COLPREFIX.L$(printf %02d $DEPTH).nDivPassings$DEPTH &
done

# LAN cache hits after diversion
for DEPTH in $(seq 1 $HEIGHT); do
  find -regex '.*/'$(makeHostDepthRegex $DEPTH)'/nfd.log.annotated.xz' | xargs xzgrep 'onContentStoreHit interest=/external/.* from='$(makeHostDepthRegex $((DEPTH-1)))'$' | wc -l > $COLPREFIX.N$(printf %02d $DEPTH).nDivHits$DEPTH &
done

wait

echo $(cat $COLPREFIX.A0.*)+$(cat $COLPREFIX.A1.*)+$(cat $COLPREFIX.A2.*) | bc > $COLPREFIX.A4.nLanExtPkts
echo $(cat $COLPREFIX.A5.*)+$(cat $COLPREFIX.A6.*)+$(cat $COLPREFIX.A7.*) | bc > $COLPREFIX.A9.nLanIntPkts
echo $(cat $COLPREFIX.B0.*)+$(cat $COLPREFIX.B1.*)+$(cat $COLPREFIX.B2.*) | bc > $COLPREFIX.B4.nWanExtPkts
echo $(cat $COLPREFIX.H0000.* $COLPREFIX.H??.* | tr '\n' '+')0 | bc > $COLPREFIX.Hzzzz.nExtNormalHitsTotal
echo $(cat $COLPREFIX.K??.* | tr '\n' '+')0 | bc > $COLPREFIX.Kzzzz.nDiversionsTotal
echo $(cat $COLPREFIX.L??.* | tr '\n' '+')0 | bc > $COLPREFIX.Lzzzz.nDivPassingsTotal
echo $(cat $COLPREFIX.N??.* | tr '\n' '+')0 | bc > $COLPREFIX.Nzzzz.nDivHitsTotal

for COL in $COLPREFIX.*; do echo $(echo $COL | cut -d. -f4) $(cat $COL); done
rm $COLPREFIX.*
