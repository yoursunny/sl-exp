#!/bin/bash
# Collect traffic counters from one NFS experiment in current directory.

if ! find */nfs-trace-client.log &>/dev/null; then
  echo 'This script should be executed in NFS experiment result directory.' >/dev/stderr
  exit 3
fi
if ! find */nfd.log.annotated.xz &>/dev/null; then
  echo 'Run mn/faceid_annotate.py '$(pwd)' first.' >/dev/stderr
  exit 4
fi

(
  echo -e 'nInterests\tnData\tnNacks\tnNfsOps\tnNfsFailures\tnFibPeak'

  xzgrep -E '\[Forwarder\] onIncoming.*/NFS/' */nfd.log.annotated.xz \
  | sed -e '/ face=app/ d' -e '/ matching=/ d' \
  | cut -d' ' -f4 | awk '
    { ++cnt[$1] }
    END { printf "%d\t%d\t%d\t", 0+cnt["onIncomingInterest"], 0+cnt["onIncomingData"], 0+cnt["onIncomingNack"] }
  '

  cut -d, -f7 */nfs-trace-client.log | awk '
    { ++cnt[$1] }
    END { printf "%d\t%d\t", 0+cnt["SUCCESS"]+cnt["FAILURE"], 0+cnt["FAILURE"] }
  '

  xzgrep 'Fib] size=' */nfd.log.annotated.xz | cut -d= -f2 \
  | awk '
    BEGIN { fibPeak = 0 }
    $1 > fibPeak { fibPeak = 0+$1 }
    END { printf "%d\n", fibPeak }
  '
) > nfs-counters.tsv
